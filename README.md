[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/idml-creator-demo)](http://www.php.net)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/d8406efa1ad3403d965683ffe1f778d1)](https://www.codacy.com/bb/wirbelwild/idml-creator-demo/dashboard) 
[![Total Downloads](https://poser.pugx.org/bitandblack/idml-creator-demo/downloads)](https://packagist.org/packages/bitandblack/idml-creator-demo)
[![License](https://poser.pugx.org/bitandblack/idml-creator-demo/license)](https://packagist.org/packages/bitandblack/idml-creator-demo)

# IDML Creator DEMO

Creates content for IDML files used by Adobe InDesign. 

__This library is a demo version with a limited number of functions. The full-functional library is private and can be used with Composer too. Visit [www.idml.dev/en/idml-creator-php.html](https://www.idml.dev/en/idml-creator-php.html) to find information about the usage and the pricing.__

## How to use 

Read more about the use of `idml` files in our [documentation](https://www.idml.dev).

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/bitandblack/idml-creator-demo). Add it to your project by running `$ composer require bitandblack/idml-creator-demo`.

## Example 

You can find an example code in the `example` folder. Call `singlePage.php` to create a basic `idml` document containing a rectangle.

## About this demo 

This library wants to give an idea how it is to create `idml` files via PHP. There are a lot of functions missing. What you __can't__ do is:

*   Create and use TextFrames, Polygons, Ovals, Images or Tables
*   Create and use Paragraph, Cell, Object or Table Styles
*   Create and use Tags, Layers or Stories
*   Use Fonts, Colors or Color Profiles

Anyway it will be enough to get an impression.

## Validation of IDML files 

Adobe provides a schema for the validation of IDML files, which can be found under [www.adobe.com](https://www.adobe.com/devnet/indesign/documentation.html). They provide a JAVA based validation only. 

We as [Bit&Black](https://www.bitandblack.com) made a port to PHP, which can be used and implemented with the help of Composer. It can be found at [packagist.org](https://packagist.org/packages/bitandblack/idml-validator) by its name `bitandblack/idml-validator`.

## Help

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).
