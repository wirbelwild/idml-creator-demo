<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML;

use BitAndBlack\Composer\Exception\VendorNotFoundException;
use BitAndBlack\Composer\VendorPath;
use BitAndBlack\Helpers\ArrayHelper;
use Doctrine\Common\Annotations\AnnotationRegistry;
use IDML\Content\BackingStory;
use IDML\Content\Container;
use IDML\Content\DesignMap;
use IDML\Content\Enum\DomVersion;
use IDML\Content\Enum\IDMLPackage;
use IDML\Content\Exception\InvalidDomStructureException;
use IDML\Content\HelperFunctionsTrait;
use IDML\Content\MimeType;
use IDML\Content\Preference;
use IDML\Content\Spread\MasterSpread;
use IDML\Content\Spread\Spread;
use IDML\Writer\ContentInterface;
use ReflectionException;

/**
 * Class Content
 *
 * @package IDML
 */
class Content implements ContentInterface
{
    /**
     * @var BackingStory[]
     */
    private array $backingStories = [];
    

    /**
     * @var Container
     */
    public $container;

    private ?DesignMap $designMap = null;

    private readonly MimeType $mimeType;

    /**
     * @var Preference[]
     */
    public $preferences = [];

    /**
     * @var Spread[]
     */
    public $spreads = [];

    /**
     * @var Container[]
     */
    private array $containers = [];

    /**
     * @var MasterSpread[]
     */
    private array $masterSpreads = [];

    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * Content constructor.
     *
     * @throws VendorNotFoundException
     * @throws ReflectionException
     */
    public function __construct()
    {
        /**
         * Enable annotations
         */
        $vendorPath = new VendorPath();
        $loader = require $vendorPath . DIRECTORY_SEPARATOR . 'autoload.php';

        /** @var callable $callableLoader */
        $callableLoader = [$loader, 'loadClass'];

        AnnotationRegistry::registerLoader($callableLoader);

        /**
         * MimeType is some kind of static
         */
        $this->mimeType = new MimeType();
    }

    /**
     * Returns the instance of the font object
     *
     * @return Content
     */
    public function addSpread(Spread ...$spread): self
    {
        foreach (ArrayHelper::getArray($spread) as $spread) {
            $this->spreads[] = $spread;
        }
        
        return $this;
    }

    /**
     * Adds a preference
     *
     * @return Content
     */
    public function addPreference(Preference ...$preference): self
    {
        foreach (ArrayHelper::getArray($preference) as $preference) {
            $this->preferences[] = $preference;
        }
        
        return $this;
    }

    /**
     * Returns all preferences
     *
     * @return Preference[]
     */
    public function getPreferences(): array
    {
        return $this->preferences;
    }

    /**
     * Returns the instance of the container object
     *
     * @return Content
     */
    public function addContainer(Container ...$container): self
    {
        foreach (ArrayHelper::getArray($container) as $container) {
            $this->containers[] = $container;
        }
        
        return $this;
    }

    /**
     * Returns all containers
     *
     * @return Container[]
     */
    public function getContainers(): array
    {
        return $this->containers;
    }

    /**
     * Adds a master spread
     *
     * @return Content
     */
    public function addMasterSpread(MasterSpread ...$masterSpread): self
    {
        foreach (ArrayHelper::getArray($masterSpread) as $masterSpread) {
            $this->masterSpreads[] = $masterSpread;
        }
        
        return $this;
    }

    /**
     * Returns all master spreads
     *
     * @return MasterSpread[]
     */
    public function getMasterSpreads(): array
    {
        return $this->masterSpreads;
    }

    /**
     * Returns the instance of the BackingStory object
     *
     * @return Content
     */
    public function addBackingStory(BackingStory ...$backingStory): self
    {
        foreach (ArrayHelper::getArray($backingStory) as $backingStory) {
            $this->backingStories[] = $backingStory;
        }
        
        return $this;
    }

    /**
     * Returns all BackingStories
     *
     * @return BackingStory[]
     */
    public function getBackingStories(): array
    {
        return $this->backingStories;
    }

    /**
     * Returns all spreads
     *
     * @return Spread[]
     */
    public function getSpreads(): array
    {
        return $this->spreads;
    }

    /**
     * Returns the whole content for the graphic.xml file
     *
     * @return string
     */
    public function getGraphicXML(): string
    {
        $domDocument = self::getNewDOMDocument();
        $element = $domDocument->createElementNS(IDMLPackage::ONE_ZERO->value, 'idPkg:Graphic');
        $element->setAttribute('DOMVersion', DomVersion::CC->value);
        $domDocument->appendChild($element);

        $content = $domDocument->saveXML();

        return $content;
    }

    /**
     * Returns the whole content for the styles.xml file
     *
     * @return string
     */
    public function getStylesXML(): string
    {
        $domDocument = self::getNewDOMDocument();
        $element = $domDocument->createElementNS(IDMLPackage::ONE_ZERO->value, 'idPkg:Styles');
        $element->setAttribute('DOMVersion', DomVersion::CC->value);
       
        /**
         * Get all character styles
         */
        $rtCharacterStyleGrp = $domDocument->createElement('RootCharacterStyleGroup');
        $rtCharacterStyleGrp->setAttribute('Self', 'RootCharacterStyleGroup1');
        
        $element->appendChild($rtCharacterStyleGrp);
        $domDocument->appendChild($element);

        /**
         * Get all paragraph styles
         */
        $rtParagraphStyleGrp = $domDocument->createElement('RootParagraphStyleGroup');
        $rtParagraphStyleGrp->setAttribute('Self', 'RootParagraphStyleGroup1');
        
        $element->appendChild($rtParagraphStyleGrp);
        
        /**
         * Get all cell styles
         */
        $rootCellStyleGroup = $domDocument->createElement('RootCellStyleGroup');
        $rootCellStyleGroup->setAttribute('Self', 'RootCellStyleGroup1');
        
        $element->appendChild($rootCellStyleGroup);
        $domDocument->appendChild($element);

        /**
         * Get all table styles
         */
        $rootTableStyleGroup = $domDocument->createElement('RootTableStyleGroup');
        $rootTableStyleGroup->setAttribute('Self', 'RootTableStyleGroup1');
        
        $element->appendChild($rootTableStyleGroup);

        /**
         * Get all object styles
         */
        $rootObjectStyleGroup = $domDocument->createElement('RootObjectStyleGroup');
        $rootObjectStyleGroup->setAttribute('Self', 'RootObjectStyleGroup1');

        $element->appendChild($rootObjectStyleGroup);

        /**
         * Get xml
         */
        $content = $domDocument->saveXML();
        
        return $content;
    }

    /**
     * Returns the whole content for the fonts.xml file
     *
     * @return string
     */
    public function getFontsXML(): string
    {
        $domDocument = self::getNewDOMDocument();
        $element = $domDocument->createElementNS(IDMLPackage::ONE_ZERO->value, 'idPkg:Fonts');
        $element->setAttribute('DOMVersion', DomVersion::CC->value);

        $domDocument->appendChild($element);
        $content = $domDocument->saveXML();
        
        return $content;
    }

    /**
     * Returns the whole content for the MasterSpread.xml file
     *
     * @return array
     * @throws InvalidDomStructureException
     */
    public function getSpreadXML(): array
    {
        $content = [];
        
        foreach ($this->getSpreads() as $spread) {
            $domDocument = self::getNewDOMDocument();
            $element = $domDocument->createElementNS(IDMLPackage::ONE_ZERO->value, 'idPkg:Spread');
            $element->setAttribute('DOMVersion', DomVersion::CC->value);
            
            $this->appendIDMLObject($element, $spread, $domDocument);

            $domDocument->appendChild($element);
            $uniqueId = (string) $spread;
            $content[$uniqueId] = $domDocument->saveXML();
        }
        
        return $content;
    }

    /**
     * Returns the whole content for the MasterSpread.xml file
     *
     * @return array
     * @throws InvalidDomStructureException
     */
    public function getMasterSpreadXML(): array
    {
        $content = [];
        
        foreach ($this->getMasterSpreads() as $masterSpread) {
            $domDocument = self::getNewDOMDocument();
            $element = $domDocument->createElementNS(IDMLPackage::ONE_ZERO->value, 'idPkg:MasterSpread');
            $element->setAttribute('DOMVersion', DomVersion::CC->value);

            $this->appendIDMLObject($element, $masterSpread, $domDocument);

            $domDocument->appendChild($element);
            $uniqueId = (string) $masterSpread;
            $content[$uniqueId] = $domDocument->saveXML();
        }
        
        return $content;
    }

    /**
     * Returns the whole content for the References.xml file
     *
     * @return string
     * @throws InvalidDomStructureException
     */
    public function getPreferencesXML(): string
    {
        $domDocument = self::getNewDOMDocument();
        $element = $domDocument->createElementNS(IDMLPackage::ONE_ZERO->value, 'idPkg:Preferences');
        $element->setAttribute('DOMVersion', DomVersion::CC->value);
        
        foreach ($this->getPreferences() as $preference) {
            $this->appendIDMLObject($element, $preference, $domDocument);
        }
        
        $domDocument->appendChild($element);
        $content = $domDocument->saveXML();
        
        return $content;
    }

    /**
     * Returns the whole content for the tags.xml file
     *
     * @return string
     */
    public function getTagsXML(): string
    {
        $domDocument = self::getNewDOMDocument();
        $element = $domDocument->createElementNS(IDMLPackage::ONE_ZERO->value, 'idPkg:Tags');
        $element->setAttribute('DOMVersion', DomVersion::CC->value);
        
        $domDocument->appendChild($element);
        $content = $domDocument->saveXML();
        
        return $content;
    }
    
    /**
     * Returns the whole content for the mimetype file
     *
     * @return string
     */
    public function getMimeType(): string
    {
        return (string) $this->mimeType;
    }

    /**
     * Returns the whole content for the container.xml file
     *
     * @return string
     * @throws InvalidDomStructureException
     */
    public function getContainerXML(): string
    {
        $domDocument = self::getNewDOMDocument();
        
        foreach ($this->getContainers() as $container) {
            $this->appendIDMLObject($domDocument, $container);
        }
        
        $content = $domDocument->saveXML();
        return $content;
    }

    /**
     * Returns the whole content for the DesignMap.xml file
     *
     * @return string
     */
    public function getDesignMapXML(): string
    {
        $this->designMap
            ->setSpreads($this->spreads)
            ->setMasterSpreads($this->masterSpreads)
        ;

        return $this->designMap->render()->saveXML();
    }

    /**
     * Returns the whole content for the backingstory.xml file
     *
     * @return string
     * @throws InvalidDomStructureException
     */
    public function getBackingStoryXML(): string
    {
        $domDocument = self::getNewDOMDocument();
        $element = $domDocument->createElementNS(IDMLPackage::ONE_ZERO->value, 'idPkg:BackingStory');
        $element->setAttribute('DOMVersion', DomVersion::CC->value);

        foreach ($this->getBackingStories() as $backingStory) {
            $this->appendIDMLObject($element, $backingStory, $domDocument);
        }

        $domDocument->appendChild($element);
        $content = $domDocument->saveXML();
        return $content;
    }

    /**
     * @return array
     */
    public function getStoryXML(): array
    {
        return [];
    }
    
    /**
     * @return \IDML\Content
     */
    public function addDesignMap(DesignMap $designMap): self
    {
        $this->designMap = $designMap;
        return $this;
    }

    public function getMetaDataXML(): string
    {
        return '';
    }
}
