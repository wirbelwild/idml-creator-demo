<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use DOMDocument;

/**
 * The NestedDOMObjectInterface defines basic methods which need to be implemented by all objects.
 *
 * @package IDML\Content
 */
interface NestedDOMObjectInterface
{
    /**
     * This method returns all property values
     *
     * @return array
     */
    public function getProperties(NestedDOMObjectInterface $class): array;

    /**
     * This method is used for creating the xml dom out of an object
     *
     * @return DOMDocument
     */
    public function render(): DOMDocument;
}
