<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use DOMDocument;

/**
 * Handles the BackingStory
 *
 * @package IDML\Content
 */
class BackingStory extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * Returns the object
     *
     * @return DOMDocument
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        $xmlElement = $domDocument->createElement('XMLElement');
        $xmlElement->setAttribute('Self', 'calbacele1');
        $xmlElement->setAttribute('MarkupTag', 'XMLTag/Root');

        $content = $domDocument->createElement('Content', '');

        $characterStyleRange = $domDocument->createElement('CharacterStyleRange');
        $characterStyleRange->setAttribute(
            'AppliedCharacterStyle',
            'CharacterStyle/$ID/[No character style]'
        );
        $characterStyleRange->appendChild($xmlElement);
        $characterStyleRange->appendChild($content);

        $paragraphStyleRange = $domDocument->createElement('ParagraphStyleRange');
        $paragraphStyleRange->setAttribute(
            'AppliedParagraphStyle',
            'ParagraphStyle/$ID/NormalParagraphStyle'
        );
        $paragraphStyleRange->appendChild($characterStyleRange);

        $xmlStory = $domDocument->createElement('XmlStory');
        $xmlStory->setAttribute('Self', 'calbacxml1');
        $xmlStory->setAttribute('AppliedTOCStyle', 'n');
        $xmlStory->setAttribute('TrackChanges', 'false');
        $xmlStory->setAttribute('StoryTitle', '$ID/');
        $xmlStory->setAttribute('AppliedNamedGrid', 'n');
        $xmlStory->appendChild($paragraphStyleRange);

        $domDocument->appendChild($xmlStory);

        return $domDocument;
    }
}
