<?php

namespace IDML\Content\Exception;

use IDML\Content\Exception;

/**
 * Class AnnotationReaderNotInitializedException
 * Annotions work only as expected when the IDML\Content class was initialized at first.
 * As long as the Doctrine Annotation Reader wants to change its logic in the future, this can be improved some day
 *
 * @package IDML\Content\Exception
 */
class AnnotationReaderNotInitializedException extends Exception
{
    /**
     * AnnotationReaderNotInitializedException constructor.
     */
    public function __construct()
    {
        parent::__construct('You need to initialize the IDML\Content class at first');
    }
}
