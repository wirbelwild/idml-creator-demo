<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Exception;

use IDML\Content\Exception;

/**
 * Class InvalidPropertyException
 *
 * @package IDML\Content\Exception
 */
class InvalidPropertyException extends Exception
{
    /**
     * InvalidPropertyException constructor.
     *
     * @param string $attribute
     */
    public function __construct(string $attribute)
    {
        parent::__construct('Can\'t use attribute "' . $attribute . '" as parameter because it\'s an array');
    }
}
