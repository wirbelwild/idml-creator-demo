<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Exception;

use IDML\Content\Exception;

/**
 * Class ColumnsException
 *
 * @package IDML\Content\Exception
 */
class ColumnsException extends Exception
{
    /**
     * ColumnsException constructor.
     *
     * @param int $number
     * @param float $bridge
     * @param float $elementWidth
     */
    public function __construct(int $number, float $bridge, float $elementWidth)
    {
        parent::__construct(
            $number . ' columns with a bridge of ' . round($bridge, 2) . ' point ' .
            'doesn\'t fit in your document with a width of ' . round($elementWidth, 2) . ' point'
        );
    }
}
