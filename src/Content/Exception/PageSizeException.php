<?php

namespace IDML\Content\Exception;

use IDML\Content\Enum\PageSize;
use IDML\Content\Enum\Unit;
use IDML\Content\Exception;

/**
 * Class PageSizeException
 *
 * @package IDML\Content\Exception
 */
class PageSizeException extends Exception
{
    /**
     * PageSizeException constructor.
     *
     * @param Unit $unit
     */
    public function __construct(Unit $unit)
    {
        $minMM = PageSize::MINIMUM->value . ' pt';
        $maxMM = PageSize::MAXIMUM->value . ' pt';

        if (Unit::MILLIMETERS === $unit) {
            $minMM = round(PageSize::MINIMUM->value * Unit::MILLIMETERS->getValue(), 3) . ' mm';
            $maxMM = round(PageSize::MAXIMUM->value * Unit::MILLIMETERS->getValue(), 3) . ' mm';
        }
         
        parent::__construct('The page size needs to be between ' . $minMM . ' and ' . $maxMM);
    }
}
