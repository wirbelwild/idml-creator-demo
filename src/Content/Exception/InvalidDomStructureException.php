<?php

namespace IDML\Content\Exception;

use IDML\Content\Exception;

/**
 * Class InvalidDomStructureException
 *
 * @package IDML\Content
 */
class InvalidDomStructureException extends Exception
{
    /**
     * InvalidDomStructureException constructor.
     */
    public function __construct()
    {
        parent::__construct('Element with invalid DOM structure found');
    }
}
