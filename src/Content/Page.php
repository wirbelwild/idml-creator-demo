<?php

/**
 * IDML-Creator
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use BitAndBlack\Helpers\ArrayHelper;
use Doctrine\Common\Annotations\AnnotationException;
use DOMDocument;
use DOMElement;
use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\Enum\PageSize;
use IDML\Content\Enum\Unit;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use IDML\Content\Exception\ColumnsException;
use IDML\Content\Exception\InvalidPropertyException;
use IDML\Content\Exception\PageSizeException;
use IDML\Content\Spread\MasterSpread;
use ReflectionException;
use Stringable;
use XSLTProcessor;

/**
 * Handles pages
 *
 * This class creates Page on the fly.
 * The code will be returned and must be send to a MasterSpread or Spread.
 *
 * @package IDML\Content
 * @see \IDML\Tests\Content\PageTest
 */
class Page extends AbstractNestedDOMObject implements NestedDOMObjectInterface, Stringable
{
    /**
     * Calculate points and millimeters
     */
    use CalculatorTrait;
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;
    /**
     * @IDMLProperty
     */
    private string $self = '';
    /**
     * @IDMLProperty
     */
    private ?string $appliedAlternateLayout = null;
    /**
     * @var string `UseMaster`
     * @IDMLProperty
     */
    private string $layoutRule = 'UseMaster';
    /**
     * @var string `IgnoreLayoutSnapshots`
     * @IDMLProperty
     */
    private ?string $snapshotBlendingMode = null;
    /**
     * @IDMLProperty
     */
    private bool $optionalPage = false;
    /**
     * @IDMLProperty
     */
    private string $geometricBounds = '0 0 0 0';
    /**
     * @IDMLProperty
     */
    private string $itemTransform = '1 0 0 1 0 0';
    /**
     * @var string `TrapPreset/$ID/kDefaultTrapStyleName`
     * @IDMLProperty
     */
    private ?string $appliedTrapPreset = null;
    /**
     * @IDMLProperty
     */
    private string $overrideList = '';
    /**
     * @IDMLProperty
     */
    private ?MasterSpread $appliedMaster = null;
    /**
     * @IDMLProperty
     */
    private string $masterPageTransform = '1 0 0 1 0 0';
    /**
     * @IDMLProperty
     */
    private string $tabOrder = '';
    /**
     * @var string `TopOutside`
     * @IDMLProperty
     */
    private ?string $gridStartingPoint = null;
    /**
     * @IDMLProperty
     */
    private bool $useMasterGrid = true;
    /**
     * Margin attributes
     *
     * @var array
     */
    private $marginAttributes = [
        'ColumnCount' => 1,
        'ColumnGutter' => 0,
        'Top' => 0,
        'Bottom' => 0,
        'Left' => 0,
        'Right' => 0,
        'ColumnDirection' => 'Horizontal',
        'ColumnsPositions' => '0 1',
    ];
    /**
     * Width and height
     */
    private array $format = [];
    /**
     * All existing TextFrames
     *
     * @var DOMDocument[]
     */
    private array $allTextFrames = [];
    /**
     * All existing rectangles
     *
     * @var DOMDocument[]
     */
    private array $allRectangles = [];
    /**
     * All existing GraphicLines
     *
     * @var DOMDocument[]
     */
    private array $allGraphicLines = [];
    /**
     * Creates a new DOMDocument instance
     *
     * @param string|null $uniqueId
     */
    public function __construct(string $uniqueId = null)
    {
        if (null === $uniqueId) {
            $uniqueId = $this->getUniqueId($this);
        }

        $this->setSelf($uniqueId);
    }
    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getSelf();
    }
    /**
     * Set the page format
     *
     * @return Page
     * @throws PageSizeException
     */
    public function setFormat(float $width, float $height, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $width = $width / $unit->getValue();
        $height = $height / $unit->getValue();
        
        if ($width < PageSize::MINIMUM->value || $width > PageSize::MAXIMUM->value
            || $height < PageSize::MINIMUM->value || $height > PageSize::MAXIMUM->value
        ) {
            throw new PageSizeException($unit);
        }
        
        $this->setGeometricBounds("0 0 $height $width");
        
        $this->format = [
            'width' => $width,
            'height' => $height,
        ];
        
        return $this;
    }
    /**
     * Set the page margin
     *
     * @return Page
     * @throws ColumnsException
     */
    public function setMargin(
        int $top,
        int $right,
        int $bottom,
        int $left,
        Unit $unit = null
    ): self {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $top = $top / $unit->getValue();
        $right = $right / $unit->getValue();
        $bottom = $bottom / $unit->getValue();
        $left = $left / $unit->getValue();
        
        $this->marginAttributes['Top'] = $top;
        $this->marginAttributes['Right'] = $right;
        $this->marginAttributes['Bottom'] = $bottom;
        $this->marginAttributes['Left'] = $left;

        /**
         * The correct position of columns can only set after the definition of the margin.
         * That's why we call the method again here; it will update the columns position.
         */
        $this->setColumns(
            $this->marginAttributes['ColumnCount'],
            $this->marginAttributes['ColumnGutter']
        );
        
        return $this;
    }
    /**
     * Set columns count and gutter
     *
     * @return Page
     * @throws ColumnsException
     */
    public function setColumns(
        int $number,
        float $bridge,
        Unit $unit = null
    ): self {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $bridge = $bridge / $unit->getValue();
        
        $this->marginAttributes['ColumnGutter'] = $bridge;
        $this->marginAttributes['ColumnCount'] = $number;
        /**
         * Width of element
         */
        $elementWidth = $this->format['width'] - $this->marginAttributes['Left'] - $this->marginAttributes['Right'];

        if ($elementWidth - ($bridge * ($number - 1)) <= 0) {
            throw new ColumnsException($number, $bridge, $elementWidth);
        }
        
        /**
         * With of single column
         */
        $columnWidth = ($elementWidth - ($bridge * ($number - 1))) / $number;

        /**
         * Set column positions
         */
        $columnsPositions = [0];
        
        for ($position = 0, $counter = 1; $counter < $number; $counter++) {
            $position += $columnWidth;
            $columnsPositions[] = $position;
            $position += $bridge;
            $columnsPositions[] = $position;
        }
        
        $columnsPositions[] = $elementWidth;
        $columnsPositions = implode(' ', $columnsPositions);
        
        $this->marginAttributes['ColumnsPositions'] = $columnsPositions;
        
        return $this;
    }
    /**
     * Adds a rectangle
     *
     * @return Page
     */
    public function addRectangle(Rectangle ...$rectangle): self
    {
        /**
         * @var Rectangle $rectangle
         */
        foreach (ArrayHelper::getArray($rectangle) as $rectangle) {
            $this->addElement('Rectangle', $rectangle);
        }

        return $this;
    }
    /**
     * addElement
     *
     * @return Page
     */
    private function addElement(string $elementName, DOMObjectInterface $element): self
    {
        $domDocument = $element->render();

        $positionRenderer = new PositionRenderer($domDocument, $this->format, $this->getSelf());
        $domDocument = $positionRenderer();
        
        switch ($elementName) {
            case 'TextFrame':
                $this->allTextFrames[] = $domDocument;
                break;
            case 'Rectangle':
                $this->allRectangles[] = $domDocument;
                break;
            case 'GraphicLine':
                $this->allGraphicLines[] = $domDocument;
                break;
        }
        
        return $this;
    }
    /**
     * Saves the paragraph style and returns them
     *
     * @return DOMDocument
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidPropertyException
     * @throws AnnotationReaderNotInitializedException
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();
        $properties = $this->getProperties($this);
        
        $tempRoot = $domDocument->createElement('TempRoot');
       
        if (null === $properties['AppliedMaster']) {
            $properties['AppliedMaster'] = 'n';
        }
        
        /**
         * Page
         */
        $page = $domDocument->createElement('Page');
        $this->setAttributes($page, $properties);

        /**
         * Page margin
         */
        $marginPreference = $domDocument->createElement('MarginPreference');
        $this->setAttributes($marginPreference, $this->marginAttributes);
        
        $page->appendChild($marginPreference);
        $tempRoot->appendChild($page);

        /**
         * Append all TextFrames
         */
        foreach ($this->allTextFrames ?? [] as $textFrame) {
            /** @var DOMElement $domElement */
            $domElement = $textFrame->documentElement;
            $tempRoot->appendChild(
                $domDocument->importNode($domElement, true)
            );
        }

        /**
         * Append all Rectangles
         */
        foreach ($this->allRectangles ?? [] as $rectangle) {
            /** @var DOMElement $domElement */
            $domElement = $rectangle->documentElement;
            $tempRoot->appendChild(
                $domDocument->importNode($domElement, true)
            );
        }

        /**
         * Append all GraphicLines
         */
        foreach ($this->allGraphicLines ?? [] as $graphicLines) {
            /** @var DOMElement $domElement */
            $domElement = $graphicLines->documentElement;
            $tempRoot->appendChild(
                $domDocument->importNode($domElement, true)
            );
        }

        /**
         * Sort elements
         */
        $xslTempSortOrder = <<<EOT
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" encoding="UTF-8" omit-xml-declaration="yes" indent="yes" cdata-section-elements="Contents"/>
<xsl:strip-space elements="*"/>
<xsl:template match="node() | @*">
    <xsl:copy>
        <xsl:apply-templates select="node() | @*">
            <xsl:sort select="@TempSortOrder"/>
        </xsl:apply-templates>
    </xsl:copy>
</xsl:template>
</xsl:stylesheet>
EOT;
        
        $domDocumentTemp = new DOMDocument();
        $domDocumentTemp->loadXML($domDocument->saveXML($tempRoot));
        
        $xslStyleSheet = new DOMDocument();
        $xslStyleSheet->loadXML($xslTempSortOrder);
        
        $xslTProcessor = new XSLTProcessor();
        $xslTProcessor->importStyleSheet($xslStyleSheet);
        $tempRootNew = $xslTProcessor->transformToXml($domDocumentTemp);
        
        $domDocumentTemp = new DOMDocument();
        $domDocumentTemp->loadXML($tempRootNew);
        
        return $domDocumentTemp;
    }
    /**
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }
    /**
     * @return Page
     */
    public function setSelf(string $self): self
    {
        $this->self = $self;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getGeometricBounds(): ?string
    {
        return $this->geometricBounds;
    }
    /**
     * @return Page
     */
    public function setGeometricBounds(string $geometricBounds): self
    {
        $this->geometricBounds = $geometricBounds;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getAppliedAlternateLayout(): ?string
    {
        return $this->appliedAlternateLayout;
    }
    /**
     * @return Page
     */
    public function setAppliedAlternateLayout(string $appliedAlternateLayout): self
    {
        $this->appliedAlternateLayout = $appliedAlternateLayout;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getLayoutRule(): ?string
    {
        return $this->layoutRule;
    }
    /**
     * @return Page
     */
    public function setLayoutRule(string $layoutRule): self
    {
        $this->layoutRule = $layoutRule;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getSnapshotBlendingMode(): ?string
    {
        return $this->snapshotBlendingMode;
    }
    /**
     * @return Page
     */
    public function setSnapshotBlendingMode(string $snapshotBlendingMode): self
    {
        $this->snapshotBlendingMode = $snapshotBlendingMode;
        return $this;
    }
    /**
     * @return bool|null
     */
    public function isOptionalPage(): ?bool
    {
        return $this->optionalPage;
    }
    /**
     * @return Page
     */
    public function setOptionalPage(bool $optionalPage): self
    {
        $this->optionalPage = $optionalPage;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getAppliedTrapPreset(): ?string
    {
        return $this->appliedTrapPreset;
    }
    /**
     * @return Page
     */
    public function setAppliedTrapPreset(string $appliedTrapPreset): self
    {
        $this->appliedTrapPreset = $appliedTrapPreset;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getOverrideList(): ?string
    {
        return $this->overrideList;
    }
    /**
     * @return Page
     */
    public function setOverrideList(string $overrideList): self
    {
        $this->overrideList = $overrideList;
        return $this;
    }
    /**
     * @return MasterSpread|null
     */
    public function getAppliedMaster(): ?MasterSpread
    {
        return $this->appliedMaster;
    }
    /**
     * @return Page
     */
    public function setAppliedMaster(MasterSpread $masterSpread): self
    {
        $this->appliedMaster = $masterSpread;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getMasterPageTransform(): ?string
    {
        return $this->masterPageTransform;
    }
    /**
     * @return Page
     */
    public function setMasterPageTransform(string $masterPageTransform): self
    {
        $this->masterPageTransform = $masterPageTransform;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getTabOrder(): ?string
    {
        return $this->tabOrder;
    }
    /**
     * @return Page
     */
    public function setTabOrder(string $tabOrder): self
    {
        $this->tabOrder = $tabOrder;
        return $this;
    }
    /**
     * @return string|null
     */
    public function getGridStartingPoint(): ?string
    {
        return $this->gridStartingPoint;
    }
    /**
     * @return Page
     */
    public function setGridStartingPoint(string $gridStartingPoint): self
    {
        $this->gridStartingPoint = $gridStartingPoint;
        return $this;
    }
    /**
     * @return bool|null
     */
    public function isUseMasterGrid(): ?bool
    {
        return $this->useMasterGrid;
    }
    /**
     * @return Page
     */
    public function setUseMasterGrid(bool $useMasterGrid): self
    {
        $this->useMasterGrid = $useMasterGrid;
        return $this;
    }
    /**
     * @return array
     */
    public function getMarginAttributes(): array
    {
        return $this->marginAttributes;
    }
    /**
     * @return Page
     */
    public function setMarginAttributes(array $marginAttributes): self
    {
        $this->marginAttributes = $marginAttributes;
        return $this;
    }
}
