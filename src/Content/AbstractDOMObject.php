<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

/**
 * The AbstractDOMObject class provides the logic for cloning the object.
 *
 * @package IDML\Content
 */
abstract class AbstractDOMObject extends AbstractNestedDOMObject implements DOMObjectInterface
{
    /**
     * @param string $self
     * @return AbstractNestedDOMObject
     */
    abstract public function setSelf(string $self);

    /**
     * @param string $name
     * @return AbstractNestedDOMObject
     */
    abstract public function setName(string $name);

    /**
     * Whenever an object gets cloned we need to create a new unique Self attribute
     * and also a new instance of DomDocument
     */
    public function __clone()
    {
        $uniqueId = $this->getUniqueId($this);
        $this
            ->setSelf($uniqueId)
            ->setName($uniqueId)
        ;
    }
}
