<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\SplineItem;

use Doctrine\Common\Annotations\AnnotationException;
use DOMDocument;
use IDML\Content\AbstractNestedDOMObject;
use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use IDML\Content\Exception\InvalidPropertyException;
use IDML\Content\HelperFunctionsTrait;
use IDML\Content\NestedDOMObjectInterface;
use ReflectionException;

/**
 * Handles the object export option
 *
 * @package IDML\Content\SplineItem
 */
class ObjectExportOption extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * @var string 'SourceXMLStructure'
     * @IDMLProperty
     */
    private ?string $altTextSourceType = null;

    /**
     * @var string 'SourceXMLStructure'
     * @IDMLProperty
     */
    private ?string $actualTextSourceType = null;

    /**
     * @var string '$ID/'
     * @IDMLProperty
     */
    private ?string $customAltText = null;

    /**
     * @var string '$ID/'
     * @IDMLProperty
     */
    private ?string $customActualText = null;

    /**
     * @var string 'TagFromStructure'
     * @IDMLProperty
     */
    private ?string $applyTagType = null;

    /**
     * @IDMLProperty
     */
    private ?bool $customImageConversion = null;

    /**
     * @var string 'JPEG'
     * @IDMLProperty
     */
    private ?string $imageConversionType = null;

    /**
     * @var string 'SizeRelativeToPageWidth'
     * @IDMLProperty
     */
    private ?string $customImageSizeOption = null;

    /**
     * @var string 'Ppi300'
     * @IDMLProperty
     */
    private ?string $imageExportResolution = null;

    /**
     * @var string 'AdaptivePalette'
     * @IDMLProperty
     */
    private ?string $gIFOptionsPalette = null;

    /**
     * @IDMLProperty
     */
    private ?bool $gIFOptionsInterlaced = null;

    /**
     * @var string 'High'
     * @IDMLProperty
     */
    private ?string $jPEGOptionsQuality = null;

    /**
     * @var string 'BaselineEncoding'
     * @IDMLProperty
     */
    private ?string $jPEGOptionsFormat = null;

    /**
     * @var string 'AlignLeft'
     * @IDMLProperty
     */
    private ?string $imageAlignment = null;

    /**
     * @var int `0`
     * @IDMLProperty
     */
    private ?int $imageSpaceBefore = null;

    /**
     * @var int `0`
     * @IDMLProperty
     */
    private ?int $imageSpaceAfter = null;

    /**
     * @IDMLProperty
     */
    private ?bool $useImagePageBreak = null;

    /**
     * @var string 'PageBreakBefore'
     * @IDMLProperty
     */
    private ?string $imagePageBreak = null;

    /**
     * @IDMLProperty
     */
    private ?bool $customImageAlignment = null;

    /**
     * @var string 'CssPixel'
     * @IDMLProperty
     */
    private ?string $spaceUnit = null;

    /**
     * @IDMLProperty
     */
    private ?bool $customLayout = null;

    /**
     * @var string 'AlignmentAndSpacing'
     * @IDMLProperty
     */
    private ?string $customLayoutType = null;

    /**
     * return the property element
     *
     * @return DOMDocument
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidPropertyException
     * @throws AnnotationReaderNotInitializedException
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        $properties = $this->getProperties($this);

        $objectExportOption = $domDocument->createElement('ObjectExportOption');
        $this->setAttributes($objectExportOption, $properties);

        $domDocument->appendChild($objectExportOption);
        return $domDocument;
    }

    /**
     * @return string|null
     */
    public function getAltTextSourceType(): ?string
    {
        return $this->altTextSourceType;
    }

    /**
     * @return ObjectExportOption
     */
    public function setAltTextSourceType(string $altTextSourceType): self
    {
        $this->altTextSourceType = $altTextSourceType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getActualTextSourceType(): ?string
    {
        return $this->actualTextSourceType;
    }

    /**
     * @return ObjectExportOption
     */
    public function setActualTextSourceType(string $actualTextSourceType): self
    {
        $this->actualTextSourceType = $actualTextSourceType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomAltText(): ?string
    {
        return $this->customAltText;
    }

    /**
     * @return ObjectExportOption
     */
    public function setCustomAltText(string $customAltText): self
    {
        $this->customAltText = $customAltText;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomActualText(): ?string
    {
        return $this->customActualText;
    }

    /**
     * @return ObjectExportOption
     */
    public function setCustomActualText(string $customActualText): self
    {
        $this->customActualText = $customActualText;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApplyTagType(): ?string
    {
        return $this->applyTagType;
    }

    /**
     * @return ObjectExportOption
     */
    public function setApplyTagType(string $applyTagType): self
    {
        $this->applyTagType = $applyTagType;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isCustomImageConversion(): ?bool
    {
        return $this->customImageConversion;
    }

    /**
     * @return ObjectExportOption
     */
    public function setCustomImageConversion(bool $customImageConversion): self
    {
        $this->customImageConversion = $customImageConversion;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageConversionType(): ?string
    {
        return $this->imageConversionType;
    }

    /**
     * @return ObjectExportOption
     */
    public function setImageConversionType(string $imageConversionType): self
    {
        $this->imageConversionType = $imageConversionType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomImageSizeOption(): ?string
    {
        return $this->customImageSizeOption;
    }

    /**
     * @return ObjectExportOption
     */
    public function setCustomImageSizeOption(string $customImageSizeOption): self
    {
        $this->customImageSizeOption = $customImageSizeOption;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageExportResolution(): ?string
    {
        return $this->imageExportResolution;
    }

    /**
     * @return ObjectExportOption
     */
    public function setImageExportResolution(string $imageExportResolution): self
    {
        $this->imageExportResolution = $imageExportResolution;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGIFOptionsPalette(): ?string
    {
        return $this->gIFOptionsPalette;
    }

    /**
     * @return ObjectExportOption
     */
    public function setGIFOptionsPalette(string $gIFOptionsPalette): self
    {
        $this->gIFOptionsPalette = $gIFOptionsPalette;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isGIFOptionsInterlaced(): ?bool
    {
        return $this->gIFOptionsInterlaced;
    }

    /**
     * @return ObjectExportOption
     */
    public function setGIFOptionsInterlaced(bool $gIFOptionsInterlaced): self
    {
        $this->gIFOptionsInterlaced = $gIFOptionsInterlaced;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getJPEGOptionsQuality(): ?string
    {
        return $this->jPEGOptionsQuality;
    }

    /**
     * @return ObjectExportOption
     */
    public function setJPEGOptionsQuality(string $jPEGOptionsQuality): self
    {
        $this->jPEGOptionsQuality = $jPEGOptionsQuality;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getJPEGOptionsFormat(): ?string
    {
        return $this->jPEGOptionsFormat;
    }

    /**
     * @return ObjectExportOption
     */
    public function setJPEGOptionsFormat(string $jPEGOptionsFormat): self
    {
        $this->jPEGOptionsFormat = $jPEGOptionsFormat;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImageAlignment(): ?string
    {
        return $this->imageAlignment;
    }

    /**
     * @return ObjectExportOption
     */
    public function setImageAlignment(string $imageAlignment): self
    {
        $this->imageAlignment = $imageAlignment;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getImageSpaceBefore(): ?int
    {
        return $this->imageSpaceBefore;
    }

    /**
     * @return ObjectExportOption
     */
    public function setImageSpaceBefore(int $imageSpaceBefore): self
    {
        $this->imageSpaceBefore = $imageSpaceBefore;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getImageSpaceAfter(): ?int
    {
        return $this->imageSpaceAfter;
    }

    /**
     * @return ObjectExportOption
     */
    public function setImageSpaceAfter(int $imageSpaceAfter): self
    {
        $this->imageSpaceAfter = $imageSpaceAfter;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isUseImagePageBreak(): ?bool
    {
        return $this->useImagePageBreak;
    }

    /**
     * @return ObjectExportOption
     */
    public function setUseImagePageBreak(bool $useImagePageBreak): self
    {
        $this->useImagePageBreak = $useImagePageBreak;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getImagePageBreak(): ?string
    {
        return $this->imagePageBreak;
    }

    /**
     * @return ObjectExportOption
     */
    public function setImagePageBreak(string $imagePageBreak): self
    {
        $this->imagePageBreak = $imagePageBreak;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isCustomImageAlignment(): ?bool
    {
        return $this->customImageAlignment;
    }

    /**
     * @return ObjectExportOption
     */
    public function setCustomImageAlignment(bool $customImageAlignment): self
    {
        $this->customImageAlignment = $customImageAlignment;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSpaceUnit(): ?string
    {
        return $this->spaceUnit;
    }

    /**
     * @return ObjectExportOption
     */
    public function setSpaceUnit(string $spaceUnit): self
    {
        $this->spaceUnit = $spaceUnit;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isCustomLayout(): ?bool
    {
        return $this->customLayout;
    }

    /**
     * @return ObjectExportOption
     */
    public function setCustomLayout(bool $customLayout): self
    {
        $this->customLayout = $customLayout;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCustomLayoutType(): ?string
    {
        return $this->customLayoutType;
    }

    /**
     * @return ObjectExportOption
     */
    public function setCustomLayoutType(string $customLayoutType): self
    {
        $this->customLayoutType = $customLayoutType;
        return $this;
    }
}
