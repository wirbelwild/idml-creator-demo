<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\SplineItem;

use Doctrine\Common\Annotations\AnnotationException;
use DOMDocument;
use IDML\Content\AbstractNestedDOMObject;
use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use IDML\Content\Exception\InvalidPropertyException;
use IDML\Content\HelperFunctionsTrait;
use IDML\Content\NestedDOMObjectInterface;
use ReflectionException;

/**
 * Handles the baseline frame grid option
 *
 * @package IDML\Content\SplineItem
 */
class BaselineFrameGridOption extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;
        
    /**
     * @IDMLProperty
     */
    private ?bool $useCustomBaselineFrameGrid = null;

    /**
     * @IDMLProperty
     */
    private ?int $startingOffsetForBaselineFrameGrid = null;

    /**
     * @var string `TopOfInset`
     * @IDMLProperty
     */
    private ?string $baselineFrameGridRelativeOption = null;

    /**
     * @IDMLProperty
     */
    private ?int $baselineFrameGridIncrement = null;

    /**
     * Returns the properties element
     *
     * @return DOMDocument
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidPropertyException
     * @throws AnnotationReaderNotInitializedException
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        $properties = $this->getProperties($this);

        $baselineFrameGridOption = $domDocument->createElement('BaselineFrameGridOption');
        $this->setAttributes($baselineFrameGridOption, $properties);

        $domDocument->appendChild($baselineFrameGridOption);
        return $domDocument;
    }

    /**
     * @return bool|null
     */
    public function isUseCustomBaselineFrameGrid(): ?bool
    {
        return $this->useCustomBaselineFrameGrid;
    }

    /**
     * @return BaselineFrameGridOption
     */
    public function setUseCustomBaselineFrameGrid(bool $useCustomBaselineFrameGrid): self
    {
        $this->useCustomBaselineFrameGrid = $useCustomBaselineFrameGrid;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStartingOffsetForBaselineFrameGrid(): ?int
    {
        return $this->startingOffsetForBaselineFrameGrid;
    }

    /**
     * @return BaselineFrameGridOption
     */
    public function setStartingOffsetForBaselineFrameGrid(int $startingOffsetForBaselineFrameGrid): self
    {
        $this->startingOffsetForBaselineFrameGrid = $startingOffsetForBaselineFrameGrid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBaselineFrameGridRelativeOption(): ?string
    {
        return $this->baselineFrameGridRelativeOption;
    }

    /**
     * @return BaselineFrameGridOption
     */
    public function setBaselineFrameGridRelativeOption(string $baselineFrameGridRelativeOption): self
    {
        $this->baselineFrameGridRelativeOption = $baselineFrameGridRelativeOption;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getBaselineFrameGridIncrement(): ?int
    {
        return $this->baselineFrameGridIncrement;
    }

    /**
     * @return BaselineFrameGridOption
     */
    public function setBaselineFrameGridIncrement(int $baselineFrameGridIncrement): self
    {
        $this->baselineFrameGridIncrement = $baselineFrameGridIncrement;
        return $this;
    }
}
