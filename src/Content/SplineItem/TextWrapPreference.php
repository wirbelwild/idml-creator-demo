<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\SplineItem;

use Doctrine\Common\Annotations\AnnotationException;
use DOMDocument;
use IDML\Content\AbstractNestedDOMObject;
use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\Enum\TextWrapMode;
use IDML\Content\Enum\TextWrapSide;
use IDML\Content\Enum\Unit;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use IDML\Content\Exception\InvalidPropertyException;
use IDML\Content\HelperFunctionsTrait;
use IDML\Content\NestedDOMObjectInterface;
use ReflectionException;

/**
 * Handles the text wrap preference
 *
 * @package IDML\Content\SplineItem
 */
class TextWrapPreference extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * @IDMLProperty
     */
    private ?bool $inverse = null;

    /**
     * @IDMLProperty
     */
    private ?bool $applyToMasterPageOnly = null;

    /**
     * @IDMLProperty
     */
    private ?TextWrapSide $textWrapSide = null;

    /**
     * @IDMLProperty
     */
    private ?TextWrapMode $textWrapMode = null;

    /**
     * Text wrap offset attributes
     *
     * @var array
     */
    private $textWrapOffset = [
        'Top' => 0,
        'Left' => 0,
        'Bottom' => 0,
        'Right' => 0,
    ];

    /**
     * Renders
     *
     * @return DOMDocument
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidPropertyException
     * @throws AnnotationReaderNotInitializedException
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();
        
        $properties = $this->getProperties($this);

        $textWrapPreference = $domDocument->createElement('TextWrapPreference');
        $this->setAttributes($textWrapPreference, $properties);

        $textWrapOffset = $domDocument->createElement('TextWrapOffset');

        foreach ($this->textWrapOffset as $key => $value) {
            $textWrapOffset->setAttribute($key, $value);
        }

        $props = $domDocument->createElement('Properties');

        /**
         * Only needed by images. Possible for text anyway?
         */
        $contourOption = $domDocument->createElement('ContourOption');
        $contourOption->setAttribute('ContourType', 'SameAsClipping');
        $contourOption->setAttribute('IncludeInsideEdges', 'false');
        $contourOption->setAttribute('ContourPathName', '$ID/');

        $props->appendChild($textWrapOffset);
        $textWrapPreference->appendChild($props);
        $textWrapPreference->appendChild($contourOption);

        $domDocument->appendChild($textWrapPreference);
        return $domDocument;
    }

    /**
     * @return bool|null
     */
    public function isInverse(): ?bool
    {
        return $this->inverse;
    }

    /**
     * @return TextWrapPreference
     */
    public function setInverse(bool $inverse): self
    {
        $this->inverse = $inverse;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isApplyToMasterPageOnly(): ?bool
    {
        return $this->applyToMasterPageOnly;
    }

    /**
     * @return TextWrapPreference
     */
    public function setApplyToMasterPageOnly(bool $applyToMasterPageOnly): self
    {
        $this->applyToMasterPageOnly = $applyToMasterPageOnly;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTextWrapSide(): ?string
    {
        return $this->textWrapSide;
    }

    /**
     * @return TextWrapPreference
     */
    public function setTextWrapSide(TextWrapSide $textWrapSide): self
    {
        $this->textWrapSide = $textWrapSide;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTextWrapMode(): ?string
    {
        return $this->textWrapMode;
    }

    /**
     * @return TextWrapPreference
     */
    public function setTextWrapMode(TextWrapMode $textWrapMode): self
    {
        $this->textWrapMode = $textWrapMode;
        return $this;
    }

    /**
     * @return \IDML\Content\SplineItem\TextWrapPreference
     */
    public function setTextWrapOffset(
        float $top,
        float $right,
        float $bottom,
        float $left,
        Unit $unit = null
    ): self {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $this->textWrapOffset = [
            'Top' => $top / $unit->getValue(),
            'Right' => $right / $unit->getValue(),
            'Bottom' => $bottom / $unit->getValue(),
            'Left' => $left / $unit->getValue(),
        ];
        return $this;
    }
}
