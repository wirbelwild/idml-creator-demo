<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\SplineItem;

use Doctrine\Common\Annotations\AnnotationException;
use DOMDocument;
use IDML\Content\AbstractNestedDOMObject;
use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use IDML\Content\Exception\InvalidPropertyException;
use IDML\Content\HelperFunctionsTrait;
use IDML\Content\NestedDOMObjectInterface;
use ReflectionException;

/**
 * Handles the transparency setting
 *
 * @package IDML\Content\SplineItem
 */
class TransparencySetting extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * @var string `Normal`|`Multiply`|`Screen`
     * @IDMLProperty
     */
    private ?string $blendMode = null;

    /**
     * @var int 100
     * @IDMLProperty
     */
    private ?int $opacity = null;

    /**
     * @IDMLProperty
     */
    private ?bool $knockoutGroup = null;

    /**
     * @IDMLProperty
     */
    private ?bool $isolateBlending = null;

    /**
     * return the property element
     *
     * @return DOMDocument
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidPropertyException
     * @throws AnnotationReaderNotInitializedException
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        $properties = $this->getProperties($this);

        $transparencySetting = $domDocument->createElement('TransparencySetting');
        $this->setAttributes($transparencySetting, $properties);

        /**
         * @todo Add attributes
         */
        $blendingSetting = $domDocument->createElement('BlendingSetting');

        $transparencySetting->appendChild($blendingSetting);

        $domDocument->appendChild($transparencySetting);
        return $domDocument;
    }

    /**
     * @return string|null
     */
    public function getBlendMode(): ?string
    {
        return $this->blendMode;
    }

    /**
     * @return TransparencySetting
     */
    public function setBlendMode(string $blendMode): self
    {
        $this->blendMode = $blendMode;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOpacity(): ?int
    {
        return $this->opacity;
    }

    /**
     * @return TransparencySetting
     */
    public function setOpacity(int $opacity): self
    {
        $this->opacity = $opacity;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isKnockoutGroup(): ?bool
    {
        return $this->knockoutGroup;
    }

    /**
     * @return TransparencySetting
     */
    public function setKnockoutGroup(bool $knockoutGroup): self
    {
        $this->knockoutGroup = $knockoutGroup;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isIsolateBlending(): ?bool
    {
        return $this->isolateBlending;
    }

    /**
     * @return TransparencySetting
     */
    public function setIsolateBlending(bool $isolateBlending): self
    {
        $this->isolateBlending = $isolateBlending;
        return $this;
    }
}
