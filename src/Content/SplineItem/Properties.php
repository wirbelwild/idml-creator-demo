<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\SplineItem;

use DOMDocument;
use IDML\Content\AbstractNestedDOMObject;
use IDML\Content\Enum\Unit;
use IDML\Content\HelperFunctionsTrait;
use IDML\Content\NestedDOMObjectInterface;

/**
 * Handles properties
 *
 * @package IDML\Content\SplineItem
 */
class Properties extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * Position
     */
    private array $position = [];

    /**
     * Size
     */
    private array $size = [];

    private bool $pathOpen = false;
    
    private array $insetSpacing = [];

    /**
     * Setting the position of the TextFrame
     *
     * @return $this
     */
    public function setPosition(float $left, float $top, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $this->position = [
            'left' => $left / $unit->getValue(),
            'top' => $top / $unit->getValue(),
        ];
        
        return $this;
    }

    /**
     * @return array
     */
    public function getPosition(): array
    {
        return $this->position;
    }

    /**
     * Setting the size of the TextFrame
     *
     * @return $this
     */
    public function setSize(float $width, float $height, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $this->size = [
            'width' => $width / $unit->getValue(),
            'height' => $height / $unit->getValue(),
        ];
        
        return $this;
    }

    /**
     * @return array
     */
    public function getSize(): array
    {
        return $this->size;
    }
        
    /**
     * Returns the property element
     *
     * @return DOMDocument
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        $props = $domDocument->createElement('Properties');

        if (!empty($this->position) && !empty($this->size)) {
            $tempPosition = $domDocument->createElement('TempPosition');
            $tempPosition->setAttribute('PositionTop', $this->position['top']);
            $tempPosition->setAttribute('PositionLeft', $this->position['left']);
            $tempPosition->setAttribute('SizeWidth', $this->size['width']);
            $tempPosition->setAttribute('SizeHeight', $this->size['height']);

            $pathPointArray = $domDocument->createElement('PathPointArray');
            $pathPointArray->appendChild($tempPosition);

            $geometryPathType = $domDocument->createElement('GeometryPathType');
            $geometryPathType->setAttribute('PathOpen', $this->boolToString($this->pathOpen));
            $geometryPathType->appendChild($pathPointArray);

            $pathGeometry = $domDocument->createElement('PathGeometry');
            $pathGeometry->appendChild($geometryPathType);
            $props->appendChild($pathGeometry);
        }

        if (!empty($this->insetSpacing)) {
            $insetSpacing = $domDocument->createElement('InsetSpacing');
            $insetSpacing->setAttribute('type', 'list');
            
            foreach ($this->insetSpacing as $spacing) {
                $listItem = $domDocument->createElement('ListItem');
                $listItem->setAttribute('type', 'unit');
                $listItem->nodeValue = $spacing;
                $insetSpacing->appendChild($listItem);
            }
            
            $props->appendChild($insetSpacing);
        }

        $domDocument->appendChild($props);

        return $domDocument;
    }

    /**
     * @return Properties
     */
    public function setPathOpen(bool $pathOpen): Properties
    {
        $this->pathOpen = $pathOpen;
        return $this;
    }

    /**
     * @return \IDML\Content\SplineItem\Properties
     */
    public function setInsetSpacing(
        float $top,
        float $right,
        float $bottom,
        float $left,
        Unit $unit = null
    ): self {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $this->insetSpacing = [
            'top' => $top / $unit->getValue(),
            'right' => $right / $unit->getValue(),
            'bottom' => $bottom / $unit->getValue(),
            'left' => $left / $unit->getValue(),
        ];

        return $this;
    }
}
