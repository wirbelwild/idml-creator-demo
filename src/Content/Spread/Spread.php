<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Spread;

use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\CalculatorTrait;
use IDML\Content\DOMObjectInterface;
use IDML\Content\Enum\Spread\PagePosition;
use IDML\Content\Enum\Unit;
use IDML\Content\HelperFunctionsTrait;

/**
 * Handles spreads
 *
 * @package IDML\Content\Spread
 * @see \IDML\Tests\Content\Spread\SpreadTest
 */
class Spread extends AbstractSpread implements DOMObjectInterface, SpreadInterface
{
    /**
     * Calculate points and millimeters
     */
    use CalculatorTrait;
    
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;
    
    /**
     * Width and height
     *
     * @var array
     */
    private $format = [
        'width' => 0,
        'height' => 0,
    ];

    /**
     * @IDMLProperty
     */
    private string $self = '';
    
    /**
     * Margin attributes
     *
     * @var array
     */
    private $marginAttributes = [
        'ColumnCount' => 1,
        'ColumnGutter' => 12,
        'Top' => 36,
        'Bottom' => 36,
        'Left' => 36,
        'Right' => 36,
        'ColumnDirection' => 'Horizontal',
        'ColumnsPositions' => '0 0',
    ];

    private array $pageAttributes = [];

    /**
     * Create a new instance
     *
     * @param string|null $uniqueId
     */
    public function __construct(string $uniqueId = null)
    {
        if (null === $uniqueId) {
            $uniqueId = $this->getUniqueId($this);
        }
        
        $this->setSelf($uniqueId);
        
        parent::__construct(
            'Spread',
            self::getNewDOMDocument(),
            [
                PagePosition::LEFT_OUTER->value => [],
                PagePosition::LEFT->value => [],
                PagePosition::LEFT_INNER->value => [],
                PagePosition::RIGHT_INNER->value => [],
                PagePosition::RIGHT->value => [],
                PagePosition::RIGHT_OUTER->value => [],
            ]
        );
    }

    /**
     * Returns the unique identifier
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getSelf();
    }

    /**
     * Set the page format
     *
     * @return Spread
     */
    public function setFormat(int $width, int $height, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $width = $width / $unit->getValue();
        $height = $height / $unit->getValue();
        
        $itemTransform = ($height / 2);
        
        $this->format = [
            'width' => $width,
            'height' => $height,
        ];
        
        $this->pageAttributes['GeometricBounds'] = "0 0 $height $width";
        $this->pageAttributes['ItemTransform'] = "1 0 0 1 0 $itemTransform";
        
        return $this;
    }

    /**
     * Set the page margin
     *
     * @return Spread
     */
    public function setMargin(
        int $top,
        int $right,
        int $bottom,
        int $left,
        Unit $unit = null
    ): self {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $top = $top / $unit->getValue();
        $right = $right / $unit->getValue();
        $bottom = $bottom / $unit->getValue();
        $left = $left / $unit->getValue();
        
        $this->marginAttributes['Top'] = $top;
        $this->marginAttributes['Right'] = $right;
        $this->marginAttributes['Bottom'] = $bottom;
        $this->marginAttributes['Left'] = $left;
        
        return $this;
    }

    /**
     * Set columns count and gutter
     *
     * @return Spread
     */
    public function setColumns(int $number, int $bridge, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $bridge = $bridge / $unit->getValue();
        
        $this->marginAttributes['ColumnCount'] = $number;
        $this->marginAttributes['ColumnGutter'] = $bridge;
        
        return $this;
    }


    /**
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * @param string $self
     * @return Spread
     */
    public function setSelf(string $self): self
    {
        $this->self = $self;
        return $this;
    }

    /**
     * @param string $name
     * @return \IDML\Content\Spread\Spread
     */
    public function setName(string $name)
    {
        trigger_error('Spreads have no names');
        return $this;
    }
}
