<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Spread;

use Doctrine\Common\Annotations\AnnotationException;
use DOMDocument;
use DOMElement;
use IDML\Content\AbstractDOMObject;
use IDML\Content\DOMObjectInterface;
use IDML\Content\Enum\Spread\PagePosition;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use IDML\Content\Exception\InvalidPropertyException;
use IDML\Content\HelperFunctionsTrait;
use IDML\Content\Page;
use IDML\Content\Preference;
use ReflectionException;

/**
 * Abstract Spread
 * This Spread holds variables and functions which are used by the Spread and the MasterSpread.
 * It's just for keeping the code clean.
 *
 * @package IDML\Content\Spread
 */
abstract class AbstractSpread extends AbstractDOMObject implements DOMObjectInterface, SpreadInterface
{
    use HelperFunctionsTrait;
    
    /**
     * Number of pages
     *
     * @var int
     */
    public $allPages = 0;

    /**
     * Position of pages
     *
     * @var array
     */
    public $pagePositions = [];

    /**
     * AbstractSpread constructor.
     * @param string $kindOfElement
     * @param DOMDocument $domDocument
     * @param array $pagePositions
     */
    public function __construct(private readonly string $kindOfElement, private readonly DOMDocument $domDocument, array $pagePositions)
    {
        $this->pagePositions = $pagePositions;
    }

    /**
     * Inserts a page into the spread
     *
     * @return AbstractSpread
     */
    public function insertPage(Page $page, PagePosition $position): self
    {
        $position = $position->value;

        if (false !== array_key_exists($position, $this->pagePositions)) {
            if (PagePosition::LEFT_OUTER == $position
                || PagePosition::RIGHT_INNER == $position
            ) {
                array_unshift($this->pagePositions[$position], $page);
            } else {
                array_push($this->pagePositions[$position], $page);
            }
        }

        $this->allPages++;

        return $this;
    }

    /**
     * Renders the spread
     *
     * @return DOMDocument
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidPropertyException
     * @throws AnnotationReaderNotInitializedException
     */
    public function render(): DOMDocument
    {
        $properties = $this->getProperties($this);

        /**
         * Create Spread element
         */
        $spread = $this->domDocument->createElement($this->kindOfElement);
        $spread->setAttribute('PageCount', (string) $this->allPages);

        if (isset(Preference::$base['singlePage']) && false === Preference::$base['singlePage']) {
            $spread->setAttribute('BindingLocation', '1');
        }
        
        $this->setAttributes($spread, $properties);

        /**
         * Append all pages in correct order
         */
        $allPages = array_merge(
            $this->pagePositions[PagePosition::LEFT_OUTER->value],
            $this->pagePositions[PagePosition::LEFT->value],
            $this->pagePositions[PagePosition::LEFT_INNER->value],
            $this->pagePositions[PagePosition::RIGHT_INNER->value],
            $this->pagePositions[PagePosition::RIGHT->value],
            $this->pagePositions[PagePosition::RIGHT_OUTER->value]
        );

        /**
         * @var Page $page
         */
        foreach ($allPages as $page) {
            /**
             * Read and insert every child node because of the page hierarchy:
             * Pages and TextFrames follow each other on the same level
             */
            $pageElements = $page->render()->documentElement;
            
            if (null === $pageElements) {
                continue;
            }
            
            $pageElements = $pageElements->childNodes;

            foreach ($pageElements as $pageElement) {
                $spread->appendChild(
                    $this->domDocument->importNode($pageElement, true)
                );
            }
        }

        /**
         * Set page positions to final absolute values
         * Get whole page with (pages may be in different sizes) and height
         */
        $pageWidthAll = 0;
        $pageHighest = 0;
        $pageWidest = 0;

        /**
         * @var DOMElement $page
         */
        foreach ($spread->getElementsByTagName('Page') as $page) {
            $geometricBounds = $page->getAttribute('GeometricBounds');
            $geometricBounds = explode(' ', $geometricBounds);
            $pageWidthAll += (float) $geometricBounds[3];

            /**
             * Get highest page
             */
            if ($geometricBounds[2] > $pageHighest) {
                $pageHighest = (float) $geometricBounds[2];
            }

            /**
             * Get widest page
             */
            if ($geometricBounds[3] > $pageWidest) {
                $pageWidest = (float) $geometricBounds[3];
            }
        }

        $sizeOfLeftPages =
            sizeof($this->pagePositions[PagePosition::LEFT_OUTER->value]) +
            sizeof($this->pagePositions[PagePosition::LEFT->value]) +
            sizeof($this->pagePositions[PagePosition::LEFT_INNER->value])
        ;

        /**
         * This may be a fix
         */
        if (0 == $sizeOfLeftPages) {
            $sizeOfLeftPages = $pageWidthAll / 2;
        }
        
        /**
         * Get left position
         * If we have a single left page by using facing pages,
         * multiply the page width to get the correct leftPosition
         */
        if (empty($this->pagePositions[PagePosition::RIGHT_OUTER->value]) &&
            empty($this->pagePositions[PagePosition::RIGHT->value]) &&
            empty($this->pagePositions[PagePosition::RIGHT_INNER->value])
        ) {
            $pageWidthAll = $pageWidthAll * 2;
            if (isset(Preference::$base['singlePage']) && false === Preference::$base['singlePage']) {
                $spread->setAttribute('BindingLocation', $sizeOfLeftPages);
            }
        } elseif (empty($this->pagePositions[PagePosition::LEFT_INNER->value]) &&
            empty($this->pagePositions[PagePosition::LEFT->value]) &&
            empty($this->pagePositions[PagePosition::LEFT_INNER->value])
        ) {
            if (isset(Preference::$base['singlePage']) && false === Preference::$base['singlePage']) {
                $spread->setAttribute('BindingLocation', '0');
            }
        } elseif (isset(Preference::$base['singlePage']) && false === Preference::$base['singlePage']) {
            $spread->setAttribute('BindingLocation', $sizeOfLeftPages);
        }

        $leftestPosition = ($pageWidthAll / 2) * -1;
        
        /**
         * Change position of each page
         *
         * @var DOMElement $pageElement
         */
        foreach ($spread->getElementsByTagName('Page') as $pageElement) {
            $geometricBounds = $pageElement->getAttribute('GeometricBounds');
            $geometricBounds = explode(' ', $geometricBounds);

            $geometricBounds = array_map(
                fn ($geometricBound) => floatval($geometricBound),
                $geometricBounds
            );

            /** Width of actual page */
            $pageWidth = $geometricBounds[3];

            /** Middle of actual page */
            $pageOriginHorizontal = $pageWidth / 2;

            /** Difference of middle to whole spread */
            $pageOrigHorizDiff = $leftestPosition + $pageOriginHorizontal;

            /** Get difference to highest page */
            if ($geometricBounds[2] < $pageHighest) {
                $differenceHeight = ($pageHighest - $geometricBounds[2]) / 2;
                $geometricBounds[0] = $differenceHeight;
                $geometricBounds[2] += $differenceHeight;
            }

            /** Get difference to widest page */
            $differenceWidth = ($pageWidest - $geometricBounds[3]) / 2;
            $geometricBounds[1] = $differenceWidth;
            $geometricBounds[3] += $differenceWidth;

            $itemTransform = $pageElement->getAttribute('ItemTransform');
            $itemTransform = explode(' ', $itemTransform);
            $itemTransform[4] = $leftestPosition - $differenceWidth;
            $itemTransform[5] = ($pageHighest / 2) * -1;
            $itemTransform = implode(' ', $itemTransform);

            $pageElement->setAttribute('ItemTransform', $itemTransform);

            $geometricBounds = implode(' ', $geometricBounds);
            $pageElement->setAttribute('GeometricBounds', $geometricBounds);
            
            /** Set TextFrame positions to final absolute values */
            $self = $pageElement->getAttribute('Self');

            /**
             * @todo Add other elements
             */
            foreach (['TextFrame', 'Rectangle', 'GraphicLine'] as $elementName) {
                $this->setAbsolutePosition($elementName, $spread, $self, $pageOrigHorizDiff);
            }

            $leftestPosition += $pageWidth;
        }

        /**
         * Delete temp tags
         */
        $tempPositionTotal = $spread->getElementsByTagname('TempPositionTotal');
        $elementToRemove = [];

        foreach ($tempPositionTotal as $page) {
            $elementToRemove[] = $page;
        }

        foreach ($elementToRemove as $tempPositionTotal) {
            $tempPositionTotal->parentNode->removeChild($tempPositionTotal);
        }

        $this->domDocument->appendChild($spread);
        return $this->domDocument;
    }

    /**
     * Setting the absolute coordinates of elements depending on their position relative to the Spread middle
     *
     * @return \IDML\Content\Spread\AbstractSpread
     */
    private function setAbsolutePosition(
        string $elementName,
        DOMElement $spread,
        string $self,
        float $pageOrigHorizDiff
    ): self {
        /**
         * @var DOMElement $spread
         * @var DOMElement $element
         * @var DOMElement $pathPointTypeElement
         */
        foreach ($spread->getElementsByTagName($elementName) as $element) {
            if ($element->hasAttribute('TempBelongsToPage')
                && $element->getAttribute('TempBelongsToPage') == $self
            ) {
                $element->removeAttribute('TempBelongsToPage');
                $this->setPathPointTypePosition($element, $pageOrigHorizDiff);

                /**
                 * If the element contains an Image, EPS or PDF, we change that position also
                 */
                $elements = [];

                foreach ($element->getElementsByTagName('Image') as $image) {
                    $elements[] = $image;
                }

                foreach ($element->getElementsByTagName('EPS') as $eps) {
                    $elements[] = $eps;
                }

                foreach ($element->getElementsByTagName('PDF') as $pdf) {
                    $elements[] = $pdf;
                }
                
                /**
                 * @var DOMElement $elementInside
                 */
                foreach ($elements as $elementInside) {
                    $this->setImagePosition(
                        $elementInside,
                        $element,
                        $pageOrigHorizDiff
                    );
                }
            }
        }
        
        return $this;
    }

    /**
     * @return \IDML\Content\Spread\AbstractSpread
     */
    private function setImagePosition(DOMElement $image, DOMElement $rectangle, float $pageOrigHorizDiff): self
    {
        /**
         * Those are the Rectangles coordinates which are basically same for the Image
         *
         * @var DOMElement $rectanglePos
         */
        $rectanglePos = $rectangle->getElementsByTagName('TempPositionTotal')->item(0);
        $rectanglePosLeft = floatval($rectanglePos->getAttribute('PositionLeftTotal')) + $pageOrigHorizDiff;
        $rectanglePosTop = floatval($rectanglePos->getAttribute('PositionTopTotal'));

        /** @var DOMElement $tempImagePosition */
        $tempImagePosition = $image->getElementsByTagName('TempImagePosition')->item(0);

        /**
         * Those are the Image left and top inside the Rectangle
         */
        $rectanglePosLeft += floatval($tempImagePosition->getAttribute('ImagePositionHorizontal'));
        $rectanglePosTop += floatval($tempImagePosition->getAttribute('ImagePositionVertical'));

        /** @var DOMElement $graphicBounds */
        $graphicBounds = $image->getElementsByTagName('GraphicBounds')->item(0);

        $imageWidth = floatval($graphicBounds->getAttribute('Right'));
        $imageHeight = floatval($graphicBounds->getAttribute('Bottom'));

        $ratio = floatval($tempImagePosition->getAttribute('ImageRatio'));

        /**
         * This value is always 1 because the image width is equal to the defined root with
         */
        $imageWidthFactor = 1;

        /**
         * The image width defines the factor, therefore image width x image ratio = root image height.
         * The image height we want divided with the root image height gives the factor
         */
        $imageHeightFactor = $imageHeight / ($imageWidth * $ratio);

        $itemTransform = $imageWidthFactor . ' 0 0 ' . $imageHeightFactor . ' ' . $rectanglePosLeft . ' ' . $rectanglePosTop;
        
        $image->setAttribute('ItemTransform', $itemTransform);
        $image->removeChild($tempImagePosition);
        
        return $this;
    }

    /**
     * Changes the values of the PathPointType's Anchor children
     */
    private function setPathPointTypePosition(DOMElement $element, float $pageOrigHorizDiff)
    {
        $positions = [];
        $positionMiddle = [];
        
        /**
         * @var DOMElement $pathPointTypeElement
         */
        foreach ($element->getElementsByTagName('PathPointType') as $pathPointTypeElement) {
            $anchor = $pathPointTypeElement->getAttribute('Anchor');
            $leftDirection = $pathPointTypeElement->getAttribute('LeftDirection');
            $rightDirection = $pathPointTypeElement->getAttribute('RightDirection');

            $anchor = explode(' ', $anchor);
            $leftDirection = explode(' ', $leftDirection);
            $rightDirection = explode(' ', $rightDirection);

            $anchor[0] += $pageOrigHorizDiff;
            $leftDirection[0] += $pageOrigHorizDiff;
            $rightDirection[0] += $pageOrigHorizDiff;

            $anchor = array_map(
                fn ($value) => (float) $value,
                $anchor
            );
            
            $positions[] = $anchor;
            
            $pathPointTypeElement->setAttribute('Anchor', implode(' ', $anchor));
            $pathPointTypeElement->setAttribute('LeftDirection', implode(' ', $leftDirection));
            $pathPointTypeElement->setAttribute('RightDirection', implode(' ', $rightDirection));
        }
        
        $endPosition = isset($positions[2])
            ? $positions[2][1]
            : $positions[1][1]
        ;
        
        $positionMiddle['top'] = $positions[0][1] - (($positions[0][1] - $endPosition) / 2);
        $positionMiddle['left'] = $positions[1][0] - (($positions[1][0] - $positions[0][0]) / 2);
        
        $itemTransform = explode(' ', $element->getAttribute('ItemTransform'));

        $itemTransform = array_map(
            fn ($value) => (float) $value,
            $itemTransform
        );
        
        if (1 !== $itemTransform[0] && 1 !== $itemTransform[3]) {
            $angle = acos($itemTransform[0]);
            $angle = rad2deg($angle);
            $angle = round($angle, 2);

            $correction = $this->getTransformCorrection($positionMiddle, $angle);

            $itemTransform[4] = $correction['left'];
            $itemTransform[5] = $correction['top'];
            
            $itemTransform = implode(' ', $itemTransform);
            
            $element->setAttribute('ItemTransform', $itemTransform);
        }
    }
    
    /**
     * This method counts the correction values for transformed elements.
     * Every element is turned around the page middle. That's why we're doing some triangle math here to find the
     * radius and the angle of the untransformed element. With that we're able to calculate the position of
     * the transformed element. The difference between positions is what we need as values to correct that.
     *
     * @return array
     */
    private function getTransformCorrection(array $positionMiddle, float $angle): array
    {
        $correction = [
            'top' => 0,
            'left' => 0,
        ];
        
        if (0 == $angle) {
            return $correction;
        }
        
        $firstElementLeft = $positionMiddle['left'];
        $firstElementTop = $positionMiddle['top'];

        $radius = sqrt(($firstElementLeft * $firstElementLeft) + ($firstElementTop * $firstElementTop));

        /** Element in the page middle doesn't need a correction */
        if (0 == $radius) {
            return $correction;
        }
        
        $q = ($firstElementLeft * $firstElementLeft) / $radius;
        $p = $radius - $q;
        $alpha = 0;

        if (0 == $firstElementTop && $firstElementLeft < 0) {
            $alpha = 90;
        } elseif (0 == $firstElementTop && $firstElementLeft > 0) {
            $alpha = 270;
        }
        
        $h = sqrt($p * $q);

        if (0 != $p && 0 != $h) {
            $alpha = rad2deg(atan($h / $p));
        }
        
        if ($firstElementLeft <= 0 && $firstElementTop <= 0) {
            /** top left */
            $secondElementLeft = $radius * -sin(deg2rad($alpha + $angle));
            $secondElementTop = $radius * -cos(deg2rad($alpha + $angle));
        } elseif ($firstElementLeft <= 0 && $firstElementTop > 0) {
            /** bottom left */
            $secondElementLeft = $radius * -sin(deg2rad($alpha - $angle));
            $secondElementTop = $radius * cos(deg2rad($alpha - $angle));
        } elseif ($firstElementLeft > 0 && $firstElementTop > 0) {
            /** bottom right */
            $secondElementLeft = $radius * sin(deg2rad($alpha + $angle));
            $secondElementTop = $radius * cos(deg2rad($alpha + $angle));
        } elseif ($firstElementLeft > 0 && $firstElementTop >= 0) {
            /** top right */
            $secondElementLeft = $radius * -sin(deg2rad($alpha - $angle));
            $secondElementTop = $radius * cos(deg2rad($alpha - $angle));
        } else {
            /** also top right */
            $secondElementLeft = $radius * sin(deg2rad($alpha - $angle));
            $secondElementTop = $radius * -cos(deg2rad($alpha - $angle));
        }
        
        $correction['left'] = ($firstElementLeft - $secondElementLeft);
        $correction['top'] = ($firstElementTop - $secondElementTop);

        return $correction;
    }
}
