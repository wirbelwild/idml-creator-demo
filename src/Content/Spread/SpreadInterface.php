<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Spread;

/**
 * This interface is used by all spread classes
 *
 * @package IDML\Content\Spread
 */
interface SpreadInterface
{
    /**
     * This method is needed so a spread object return its name
     *
     * @return string
     */
    public function __toString(): string;
}
