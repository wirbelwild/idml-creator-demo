<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Spread;

use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\CalculatorTrait;
use IDML\Content\DOMObjectInterface;
use IDML\Content\Enum\Spread\PagePosition;
use IDML\Content\HelperFunctionsTrait;

/**
 * Handles master spreads
 *
 * @package IDML\Content\Spread
 */
class MasterSpread extends AbstractSpread implements DOMObjectInterface, SpreadInterface
{
    /**
     * Calculate points and millimeters
     */
    use CalculatorTrait;
    
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * @IDMLProperty
     */
    private string $self = '';

    /**
     * @IDMLProperty
     */
    private string $appliedAlternateLayout = 'n';

    /**
     * @var string `Off`
     * @IDMLProperty
     */
    private ?string $layoutRule = null;

    /**
     * @var string `IgnoreLayoutSnapshots`
     * @IDMLProperty
     */
    private ?string $snapshotBlendingMode = null;

    /**
     * @IDMLProperty
     */
    private bool $optionalPage = false;

    /**
     * @IDMLProperty
     */
    private string $geometricBounds = '0 0 0 0';

    /**
     * @IDMLProperty
     */
    private string $itemTransform = '1 0 0 1 0 0';

    /**
     * @IDMLProperty
     */
    private ?string $name = null;
    
    /**
     * @IDMLProperty
     */
    private string $namePrefix = 'A';

    /**
     * @var string `TrapPreset/$ID/kDefaultTrapStyleName`
     * @IDMLProperty
     */
    private ?string $appliedTrapPreset = null;

    /**
     * @IDMLProperty
     */
    private ?string $overrideList = null;

    /**
     * @IDMLProperty
     */
    private string $appliedMaster = 'n';

    /**
     * @IDMLProperty
     */
    private string $masterPageTransform = '1 0 0 1 0 0';

    /**
     * @IDMLProperty
     */
    private string $tabOrder = '';

    /**
     * @var string `TopOutside`
     * @IDMLProperty
     */
    private ?string $gridStartingPoint = null;

    /**
     * @IDMLProperty
     */
    private bool $useMasterGrid = true;
    
    /**
     * If we have a single page or not
     */
    private bool $singlePage = true;

    /**
     * The base name of a page
     *
     * @IDMLProperty
     */
    private string $baseName;

    /**
     * Create a new instance
     *
     * @param string|null $uniqueId
     */
    public function __construct(string $uniqueId = null)
    {
        if (null === $uniqueId) {
            $uniqueId = $this->getUniqueId($this);
        }

        $this
            ->setSelf($uniqueId)
            ->setName($uniqueId)
            ->setBaseName($uniqueId)
        ;
        
        parent::__construct(
            'MasterSpread',
            self::getNewDOMDocument(),
            [
                PagePosition::LEFT_OUTER->value => [],
                PagePosition::LEFT->value => [],
                PagePosition::LEFT_INNER->value => [],
                PagePosition::RIGHT_INNER->value => [],
                PagePosition::RIGHT->value => [],
                PagePosition::RIGHT_OUTER->value => [],
            ]
        );
    }
    
    /**
     * Returns the unique identifier
     *
     * @return string
     */
    public function __toString(): string
    {
        return $this->getSelf();
    }
    
    /**
     * Set single or multi page information
     *
     * @return MasterSpread
     */
    public function setSinglePage(bool $bool = true): self
    {
        $this->singlePage = $bool;
        return $this;
    }
    
    /**
     * Set the pages base name
     *
     * @return MasterSpread
     */
    public function setBaseName(string $baseName): self
    {
        $this->baseName = $baseName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSelf(): string
    {
        return $this->self;
    }

    /**
     * @param string $self
     * @return MasterSpread
     */
    public function setSelf(string $self): self
    {
        $this->self = $self;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAppliedAlternateLayout(): ?string
    {
        return $this->appliedAlternateLayout;
    }

    /**
     * @return MasterSpread
     */
    public function setAppliedAlternateLayout(string $appliedAlternateLayout): self
    {
        $this->appliedAlternateLayout = $appliedAlternateLayout;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLayoutRule(): ?string
    {
        return $this->layoutRule;
    }

    /**
     * @return MasterSpread
     */
    public function setLayoutRule(string $layoutRule): self
    {
        $this->layoutRule = $layoutRule;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSnapshotBlendingMode(): ?string
    {
        return $this->snapshotBlendingMode;
    }

    /**
     * @return MasterSpread
     */
    public function setSnapshotBlendingMode(string $snapshotBlendingMode): self
    {
        $this->snapshotBlendingMode = $snapshotBlendingMode;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isOptionalPage(): ?bool
    {
        return $this->optionalPage;
    }

    /**
     * @return MasterSpread
     */
    public function setOptionalPage(bool $optionalPage): self
    {
        $this->optionalPage = $optionalPage;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGeometricBounds(): ?string
    {
        return $this->geometricBounds;
    }

    /**
     * @return MasterSpread
     */
    public function setGeometricBounds(string $geometricBounds): self
    {
        $this->geometricBounds = $geometricBounds;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return MasterSpread
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAppliedTrapPreset(): ?string
    {
        return $this->appliedTrapPreset;
    }

    /**
     * @return MasterSpread
     */
    public function setAppliedTrapPreset(string $appliedTrapPreset): self
    {
        $this->appliedTrapPreset = $appliedTrapPreset;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOverrideList(): ?string
    {
        return $this->overrideList;
    }

    /**
     * @return MasterSpread
     */
    public function setOverrideList(string $overrideList): self
    {
        $this->overrideList = $overrideList;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAppliedMaster(): ?string
    {
        return $this->appliedMaster;
    }

    /**
     * @return MasterSpread
     */
    public function setAppliedMaster(string $appliedMaster): self
    {
        $this->appliedMaster = $appliedMaster;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMasterPageTransform(): ?string
    {
        return $this->masterPageTransform;
    }

    /**
     * @return MasterSpread
     */
    public function setMasterPageTransform(string $masterPageTransform): self
    {
        $this->masterPageTransform = $masterPageTransform;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTabOrder(): ?string
    {
        return $this->tabOrder;
    }

    /**
     * @return MasterSpread
     */
    public function setTabOrder(string $tabOrder): self
    {
        $this->tabOrder = $tabOrder;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGridStartingPoint(): ?string
    {
        return $this->gridStartingPoint;
    }

    /**
     * @return MasterSpread
     */
    public function setGridStartingPoint(string $gridStartingPoint): self
    {
        $this->gridStartingPoint = $gridStartingPoint;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function isUseMasterGrid(): ?bool
    {
        return $this->useMasterGrid;
    }

    /**
     * @return MasterSpread
     */
    public function setUseMasterGrid(bool $useMasterGrid): self
    {
        $this->useMasterGrid = $useMasterGrid;
        return $this;
    }

    /**
     * @return string
     */
    public function getNamePrefix(): string
    {
        return $this->namePrefix;
    }

    /**
     * @return MasterSpread
     */
    public function setNamePrefix(string $namePrefix): self
    {
        $this->namePrefix = $namePrefix;
        return $this;
    }
}
