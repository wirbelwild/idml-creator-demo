<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use DOMDocument;
use IDML\Content\Enum\PageSize;
use IDML\Content\Enum\Unit;
use IDML\Content\Exception\PageSizeException;

/**
 * Handles the preference
 *
 * @package IDML\Content
 * @see \IDML\Tests\Content\PreferenceTest
 */
class Preference extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Width and height of a document
     *
     * @var array
     */
    private $format = [
        'width' => 0,
        'height' => 0,
    ];
    
    /**
     * The bleed
     *
     * @var float
     */
    private $bleed = 0;
    
    /**
     * If we have a single page or not
     */
    private bool $singlePage = false;
    
    /**
     * If we have a single page or not
     * This one is static because it needs to be accessed by the Spread class
     *
     * @var array
     */
    public static $base = [];
    
    /**
     * Calculate points and millimeters
     */
    use CalculatorTrait;
    
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * Sets document Format
     *
     * @return Preference
     * @throws PageSizeException
     */
    public function setDocumentFormat(float $width, float $height, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }
        
        $width = $width / $unit->getValue();
        $height = $height / $unit->getValue();

        if ($width < PageSize::MINIMUM->value || $width > PageSize::MAXIMUM->value
            || $height < PageSize::MINIMUM->value || $height > PageSize::MAXIMUM->value
        ) {
            throw new PageSizeException($unit);
        }
        
        $this->format = [
            'width' => $width,
            'height' => $height,
        ];
        
        return $this;
    }

    /**
     * Sets document Bleed
     *
     * @return Preference
     */
    public function setDocumentBleed(float $size, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $this->bleed = $size / $unit->getValue();
        return $this;
    }
    
    /**
     * Set single or multi page information
     *
     * @return Preference
     */
    public function setSinglePage(bool $isSingle): self
    {
        $this->singlePage = $isSingle;
        self::$base['singlePage'] = $isSingle;
        return $this;
    }

    /**
     * Save
     *
     * @return DOMDocument
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        $documentPreference = $domDocument->createElement('DocumentPreference');
        $documentPreference->setAttribute('PageHeight', (string) $this->format['height']);
        $documentPreference->setAttribute('PageWidth', (string) $this->format['width']);
        $documentPreference->setAttribute('DocumentBleedTopOffset', (string) $this->bleed);
        $documentPreference->setAttribute('DocumentBleedBottomOffset', (string) $this->bleed);
        $documentPreference->setAttribute('DocumentBleedOutsideOrRightOffset', (string) $this->bleed);
        $documentPreference->setAttribute('DocumentBleedInsideOrLeftOffset', (string) $this->bleed);
        $documentPreference->setAttribute('OverprintBlack', 'true');
        $documentPreference->setAttribute('PageBinding', 'LeftToRight');

        /**
         * Set if we want facing pages or not: singlePage true means facingPage false
         */
        $documentPreference->setAttribute('FacingPages', (($this->singlePage) ? 'false' : 'true'));

        /**
         * Append to root
         */
        $domDocument->appendChild($documentPreference);

        return $domDocument;
    }
}
