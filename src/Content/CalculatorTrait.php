<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

/**
 * Converts pica points to millimeters and millimeters to pica points
 *
 * @package IDML\Content
 * @todo Remove
 */
trait CalculatorTrait
{
    /**
     * The factor to calculate mm and points
     *
     * @var float
     */
    private static $mmPointFactor = 0.3527777778;
    
    /**
     * Returns a number in points
     *
     * @return float
     */
    public static function getMM2Point(mixed $millimeter): float
    {
        $millimeter = floatval($millimeter);
        return $millimeter / self::$mmPointFactor;
    }

    /**
     * Returns a number in millimeters
     *
     * @return float
     */
    public static function getPoint2MM(mixed $number): float
    {
        $number = floatval($number);
        return $number * self::$mmPointFactor;
    }
}
