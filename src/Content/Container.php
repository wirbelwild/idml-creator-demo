<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use DOMDocument;

/**
 * Handles containers
 *
 * You don't need to set properties or something else in this class.
 * Just add the tag object and save it
 *
 * @package IDML\Content
 */
class Container extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * Saves the tags to the array
     *
     * @return DOMDocument
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        $rootFile = $domDocument->createElement('rootfile');
        $rootFile->setAttribute('full-path', 'designmap.xml');
        $rootFile->setAttribute('media-type', 'text/xml');

        $rootFiles = $domDocument->createElement('rootfiles');

        $container = $domDocument->createElement('container');
        $container->setAttribute('version', '1.0');
        $container->setAttribute('xmlns', 'urn:oasis:names:tc:opendocument:xmlns:container');

        $rootFiles->appendChild($rootFile);
        $container->appendChild($rootFiles);
        $domDocument->appendChild($container);

        return $domDocument;
    }
}
