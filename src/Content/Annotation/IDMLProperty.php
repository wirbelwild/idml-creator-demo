<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Annotation;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * The IDMLProperty is used for all object properties which should be used in the xml element later
 *
 * @Annotation
 * @Target("PROPERTY")
 */
final class IDMLProperty
{
}
