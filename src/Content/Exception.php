<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

/**
 * Class Exception
 * @package IDML\Content
 */
class Exception extends \IDML\Exception
{
    /**
     * Exception constructor.
     *
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}
