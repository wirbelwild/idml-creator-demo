<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Enum;

enum StrokeAlignment: string
{
    case INSIDE_ALIGNMENT = 'InsideAlignment';
    case OUTSIDE_ALIGNMENT = 'OutsideAlignment';
    case CENTER_ALIGNMENT = 'CenterAlignment';
}
