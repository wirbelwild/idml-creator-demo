<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Enum\Spread;

enum PagePosition: string
{
    case LEFT_OUTER = 'leftOuter';
    case LEFT = 'left';
    case LEFT_INNER = 'leftInner';
    case RIGHT_INNER = 'rightInner';
    case RIGHT = 'right';
    case RIGHT_OUTER = 'rightOuter';
}
