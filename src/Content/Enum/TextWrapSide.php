<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Enum;

enum TextWrapSide: string
{
    case BOTH_SIDES = 'BothSides';
    case LEFT_SIDE = 'LeftSide';
    case RIGHT_SIDE = 'RightSide';
    case SIDE_TOWARDS_SPINE = 'SideTowardsSpine';
    case SIDE_AWAY_FROM_SPINE = 'SideAwayFromSpine';
    case LARGEST_AREA = 'LargestArea';
}
