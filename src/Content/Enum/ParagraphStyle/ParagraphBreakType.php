<?php

/**
 * IDML-Creator
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Enum\ParagraphStyle;

enum ParagraphBreakType: string
{
    case NEXT_COLUMN = 'NextColumn';
    case NEXT_FRAME = 'NextFrame';
    case NEXT_PAGE = 'NextPage';
    case NEXT_ODD_PAGE = 'NextOddPage';
    case NEXT_EVEN_PAGE = 'NextEvenPage';
}
