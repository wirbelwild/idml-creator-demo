<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content\Enum\Content;

enum Language: string
{
    case NO_LANGUAGE = '[No Language]';
    case BN_IN = 'bn_IN';
    case GU_IN = 'gu_IN';
    case HI_IN = 'hi_IN';
    case KN_IN = 'kn_IN';
    case ML_IN = 'ml_IN';
    case MR_IN = 'mr_IN';
    case OR_IN = 'or_IN';
    case PA_IN = 'pa_IN';
    case TA_IN = 'ta_IN';
    case TE_IN = 'te_IN';
    case BULGARIAN = 'Bulgarian';
    case CATALAN = 'Catalan';
    case CZECH = 'Czech';
    case DANISH = 'Danish';
    case GERMAN_SWISS = 'German: Swiss';
    case DE_CH_2006 = 'de_CH_2006';
    case GERMAN_REFORMED = 'German: Reformed';
    case DE_DE_2006 = 'de_DE_2006';
    case GREEK = 'Greek';
    case ENGLISH_CANADIAN = 'English: Canadian';
    case ENGLISH_UK = 'English: UK';
    case ENGLISH_USA = 'English: USA';
    case ENGLISH_USA_MEDICAL = 'English: USA Medical';
    case SPANISH_CASTILIAN = 'Spanish: Castilian';
    case ESTONIAN = 'Estonian';
    case FINNISH = 'Finnish';
    case FRENCH_CANADIAN = 'French: Canadian';
    case FRENCH = 'French';
    case CROATIAN = 'Croatian';
    case HUNGARIAN = 'Hungarian';
    case ITALIAN = 'Italian';
    case LITHUANIAN = 'Lithuanian';
    case LATVIAN = 'Latvian';
    case NORWEGIAN_BOKMAL = 'Norwegian: Bokmal';
    case DUTCH = 'Dutch';
    case NL_NL_2005 = 'nl_NL_2005';
    case NORWEGIAN_NYNORSK = 'Norwegian: Nynorsk';
    case POLISH = 'Polish';
    case PORTUGUESE_BRAZILIAN = 'Portuguese: Brazilian';
    case PORTUGUESE_ORTHOGRAPHIC_AGREEMENT = 'Portuguese: Orthographic Agreement';
    case PORTUGUESE = 'Portuguese';
    case ROMANIAN = 'Romanian';
    case RUSSIAN = 'Russian';
    case SLOVAK = 'Slovak';
    case SLOVENIAN = 'Slovenian';
    case SWEDISH = 'Swedish';
    case TURKISH = 'Turkish';
    case UKRAINIAN = 'Ukrainian';
    case ENGLISH_USA_LEGAL = 'English: USA Legal';
    case GERMAN_TRADITIONAL = 'German: Traditional';
    case HEBREW = 'Hebrew';
    case ARABIC = 'Arabic';
}
