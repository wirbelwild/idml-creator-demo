<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use IDML\Content\Enum\DomVersion;
use Stringable;

/**
 * Class Aid
 *
 * @package IDML\Content
 */
class Aid implements Stringable
{
    private readonly int $style;

    private readonly string $type;

    private readonly string $readerVersion;

    private readonly int $featureSet;

    private readonly string $product;

    /**
     * Aid constructor.
     *
     * @param int|null $style
     * @param string|null $type
     * @param float|null $readerVersion
     * @param int|null $featureSet
     * @param string|null $product
     */
    public function __construct(
        int $style = null,
        string $type = null,
        float $readerVersion = null,
        int $featureSet = null,
        string $product = null
    ) {
        $this->style = $style ?? 50;
        $this->type = $type ?? 'document';
        $this->readerVersion = number_format(
            $readerVersion ?? 6.0,
            1,
            '.',
            ''
        );
        $this->featureSet = $featureSet ?? 257;
        $this->product = $product ?? DomVersion::CC->value . '(419)';
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        $attributes = [
            'style' => $this->style,
            'type' => $this->type,
            'readerVersion' => $this->readerVersion,
            'featureSet' => $this->featureSet,
            'product' => $this->product,
        ];
        
        $attributes = array_map(
            fn ($key, $attribute) => $key . '="' . $attribute . '"',
            array_keys($attributes),
            array_values($attributes)
        );
        
        $attributes = implode(' ', $attributes);

        return $attributes;
    }
}
