<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use Stringable;

/**
 * Handles the MimeType
 * You don't need to set properties or something else in this class.
 *
 * @package IDML\Content
 */
class MimeType implements Stringable
{
    /**
     * Returns the array of all tags
     *
     * @return string
     */
    public function __toString(): string
    {
        return 'application/vnd.adobe.indesign-idml-package';
    }
}
