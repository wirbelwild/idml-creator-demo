<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use DOMDocument;
use DOMElement;

/**
 * This class renders the position of an object
 *
 * @package IDML\Content
 */
class PositionRenderer
{
    private array $pageFormat;

    /**
     * @var string
     */
    private $elementName;

    /**
     * @param array|null $pageFormat
     * @param string|null $parentPageId
     */
    public function __construct(
        private readonly DOMDocument $domDocument,
        array $pageFormat = null,
        private readonly ?string $parentPageId = null
    ) {
        if (null === $pageFormat) {
            $pageFormat = [
                'width' => 0,
                'height' => 0,
            ];
        }
        $this->elementName = $domDocument->childNodes[0]->tagName;
        $this->pageFormat = $pageFormat;
    }

    /**
     * @return \DOMDocument
     */
    public function __invoke(): DOMDocument
    {
        $positionTop = 0;
        $positionLeft = 0;
        $sizeWidth = 0;
        $sizeHeight = 0;

        /**
         * Get position and size information
         *
         * @var DOMElement $element
         */
        foreach ($this->domDocument->getElementsByTagName('TempPosition') as $element) {
            $positionTop = floatval($element->getAttribute('PositionTop'));
            $positionLeft = floatval($element->getAttribute('PositionLeft'));
            $sizeWidth = floatval($element->getAttribute('SizeWidth'));
            $sizeHeight = floatval($element->getAttribute('SizeHeight'));
            break;
        }

        $pageMiddleHorizontal = $this->pageFormat['width'] / 2;
        $pageMiddleVertical = $this->pageFormat['height'] / 2;

        $anchorPosition = [
            'topLeft' => [
                'x' => (0 - $pageMiddleHorizontal + $positionLeft + $sizeWidth),
                'y' => (0 - $pageMiddleVertical + $positionTop),
            ],
            'topRight' => [
                'x' => (0 - $pageMiddleHorizontal + $positionLeft),
                'y' => (0 - $pageMiddleVertical + $positionTop),
            ],
            'bottomLeft' => [
                'x' => (0 - $pageMiddleHorizontal + $positionLeft),
                'y' => (0 - $pageMiddleVertical + $positionTop + $sizeHeight),
            ],
            'bottomRight' => [
                'x' => (0 - $pageMiddleHorizontal + $positionLeft + $sizeWidth),
                'y' => (0 - $pageMiddleVertical + $positionTop + $sizeHeight),
            ],
        ];

        /**
         * Delete temp tag and append correct PathPointType tags
         *
         * @var DOMElement $element
         */
        $element = $this->domDocument->getElementsByTagName('PathPointArray')[0];
        $element->nodeValue = '';
        $tempPositionTotal = $this->domDocument->createElement('TempPositionTotal');
        $tempPositionTotal->setAttribute('PositionTopTotal', (string) $anchorPosition['topLeft']['y']);
        $tempPositionTotal->setAttribute('PositionLeftTotal', (string) $anchorPosition['bottomLeft']['x']);
        $tempPositionTotal->setAttribute('SizeWidth', (string) $sizeWidth);
        $tempPositionTotal->setAttribute('SizeHeight', (string) $sizeHeight);
        $element->appendChild($tempPositionTotal);

        /**
         * Removes two unused anchors for GraphicLines
         */
        if ('GraphicLine' == $this->elementName) {
            unset($anchorPosition['topLeft'], $anchorPosition['bottomLeft']);
        }

        foreach ($anchorPosition as $value) {
            $pathPointType = $this->domDocument->createElement('PathPointType');
            $pathPointType->setAttribute('Anchor', implode(' ', $value));
            $pathPointType->setAttribute('LeftDirection', implode(' ', $value));
            $pathPointType->setAttribute('RightDirection', implode(' ', $value));
            $element->appendChild($pathPointType);
        }

        /**
         * Add attribute to identify its page
         */
        if (null !== $this->parentPageId) {
            foreach ($this->domDocument->getElementsByTagName($this->elementName) as $element) {
                $element->setAttribute('TempBelongsToPage', $this->parentPageId);
                break;
            }
        }

        return $this->domDocument;
    }
}
