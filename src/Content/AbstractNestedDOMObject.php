<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use BitAndBlack\Helpers\StringHelper;
use Doctrine\Common\Annotations\AnnotationException;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

/**
 * The AbstractNestedDOMObject class provides the handling of properties so we don't need to repeat that in every class.
 *
 * @package IDML\Content
 */
abstract class AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    use HelperFunctionsTrait;

    /**
     * Returns all properties of a class which should be used for the xml element
     * The properties to use have the annotation IDMLProperty
     *
     * @param NestedDOMObjectInterface $class The class from which you want to receive properties
     * @return array
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws AnnotationReaderNotInitializedException
     */
    public function getProperties(NestedDOMObjectInterface $class): array
    {
        if (!class_exists(AnnotationRegistry::class, false)) {
            throw new AnnotationReaderNotInitializedException();
        }
        
        $propertiesAll = [];

        $reflect = new ReflectionClass($class);
        $properties = $reflect->getProperties();

        foreach ($properties as $property) {
            $reflectionProperty = $reflect->getProperty($property->getName());
            $reflectionProperty->setAccessible(true);

            $propertyName = $property->getName();
            $propertyValue = $reflectionProperty->getValue($class);

            $annotationReader = new AnnotationReader();

            $reflectionProperty = new ReflectionProperty($class, $propertyName);
            $propertyAnnotations = $annotationReader->getPropertyAnnotations($reflectionProperty);

            if (!empty($propertyAnnotations) && $propertyAnnotations[0] instanceof IDMLProperty) {
                $propertyName = StringHelper::mbUcFirst($propertyName);
                $propertiesAll[$propertyName] = $propertyValue;
            }
        }

        return $propertiesAll;
    }
}
