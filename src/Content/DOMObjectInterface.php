<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

/**
 * The DOMObjectInterface is made for the "bigger" DOMObjects like a CellStyle or a Rectangle.
 * They can contain one or more of the "lower" NestedDOMObjects, that's why they're called nested.
 * They all need to have a Self and a Name attribute.
 *
 * @package IDML\Content
 */
interface DOMObjectInterface extends NestedDOMObjectInterface
{
    /**
     * This method should provides a proper handling of the unique Self attribute and also
     * the DomDocument which needs to be renewed
     */
    public function __clone();

    /**
     * @param string $self
     * @return AbstractNestedDOMObject
     */
    public function setSelf(string $self);

    /**
     * @param string $name
     * @return AbstractNestedDOMObject
     */
    public function setName(string $name);
}
