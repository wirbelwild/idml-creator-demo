<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use DOMDocument;
use IDML\Content\Enum\DomVersion;
use IDML\Content\Enum\IDMLPackage;
use IDML\Content\Spread\MasterSpread;
use IDML\Content\Spread\Spread;

/**
 * Handles the DesignMap
 *
 * You don't need to set properties or something else in this class.
 * Just add the tag object and save it
 * IndexingSortOption important?
 *
 * @package IDML\Content
 */
class DesignMap extends AbstractNestedDOMObject implements NestedDOMObjectInterface
{
    /**
     * Useful methods
     */
    use HelperFunctionsTrait;
    
    /**
     * @var Spread[]
     */
    private ?array $spreads = null;
    
    /**
     * @var MasterSpread[]
     */
    private ?array $masterSpreads = null;

    private ?Aid $aid = null;

    /**
     * @param Spread[] $spreads
     * @return DesignMap
     */
    public function setSpreads(array $spreads): self
    {
        $this->spreads = $spreads;
        return $this;
    }

    /**
     * @return Spread[]
     */
    private function getSpreads(): array
    {
        return $this->spreads ?? [];
    }

    /**
     * @param MasterSpread[] $masterSpreads
     * @return DesignMap
     */
    public function setMasterSpreads(array $masterSpreads): self
    {
        $this->masterSpreads = $masterSpreads;
        return $this;
    }

    /**
     * @return MasterSpread[]
     */
    private function getMasterSpreads(): array
    {
        return $this->masterSpreads ?? [];
    }

    /**
     * Saves the tags to the array
     *
     * @return DOMDocument
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        /**
         * Append processing instruction as second line here
         * InDesign needs that line and without that the IDML document will be invalid
         */
        $aid = $domDocument->createProcessingInstruction(
            'aid',
            $this->getAid() ?? new Aid()
        );
        $domDocument->appendChild($aid);

        $document = $domDocument->createElement('Document');
        $document->setAttribute('xmlns:idPkg', IDMLPackage::ONE_ZERO->value);
        $document->setAttribute('Self', 'd');
        $document->setAttribute('DOMVersion', DomVersion::CC->value);
        $document->setAttribute('SolidColorIntent', 'UseColorSettings');
        $document->setAttribute('AfterBlendingIntent', 'UseColorSettings');
        $document->setAttribute('DefaultImageIntent', 'UseColorSettings');
        $document->setAttribute('RGBPolicy', 'PreserveEmbeddedProfiles');
        $document->setAttribute('CMYKPolicy', 'PreserveEmbeddedProfiles');
        $document->setAttribute('AccurateLABSpots', 'false');
        
        $domDocument->appendChild($document);
        
        $keyValuePair = $domDocument->createElement('KeyValuePair');
        $keyValuePair->setAttribute('Key', 'kAdobeDPS_Version');
        $keyValuePair->setAttribute('Value', '2');
        
        $label = $domDocument->createElement('Label');
        $label->appendChild($keyValuePair);
        
        $properties = $domDocument->createElement('Properties');
        $properties->appendChild($label);
        
        $document->appendChild($properties);
        
        $element = $domDocument->createElement('idPkg:Graphic');
        $element->setAttribute('src', 'Resources/Graphic.xml');
        $document->appendChild($element);
        
        $element = $domDocument->createElement('idPkg:Fonts');
        $element->setAttribute('src', 'Resources/Fonts.xml');
        $document->appendChild($element);
        
        $element = $domDocument->createElement('idPkg:Styles');
        $element->setAttribute('src', 'Resources/Styles.xml');
        $document->appendChild($element);
        
        $element = $domDocument->createElement('idPkg:Preferences');
        $element->setAttribute('src', 'Resources/Preferences.xml');
        $document->appendChild($element);
        
        $element = $domDocument->createElement('idPkg:Tags');
        $element->setAttribute('src', 'XML/Tags.xml');
        $document->appendChild($element);
        
        foreach ($this->getMasterSpreads() as $masterSpread) {
            $element = $domDocument->createElement('idPkg:MasterSpread');
            $element->setAttribute('src', 'MasterSpreads/MasterSpread_' . $masterSpread . '.xml');
            $document->appendChild($element);
        }
        
        foreach ($this->getSpreads() as $spread) {
            $element = $domDocument->createElement('idPkg:Spread');
            $element->setAttribute('src', 'Spreads/Spread_' . $spread . '.xml');
            $document->appendChild($element);
        }
        
        $element = $domDocument->createElement('idPkg:BackingStory');
        $element->setAttribute('src', 'XML/BackingStory.xml');
        
        $document->appendChild($element);
        
        return $domDocument;
    }

    /**
     * @return Aid|null
     */
    public function getAid(): ?Aid
    {
        return $this->aid;
    }

    /**
     * @return \IDML\Content\DesignMap
     */
    public function setAid(Aid $aid): self
    {
        $this->aid = $aid;
        return $this;
    }
}
