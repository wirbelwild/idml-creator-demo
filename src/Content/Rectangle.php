<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use Doctrine\Common\Annotations\AnnotationException;
use DOMDocument;
use IDML\Content\Annotation\IDMLProperty;
use IDML\Content\Enum\StrokeAlignment;
use IDML\Content\Enum\TextWrapMode;
use IDML\Content\Enum\TextWrapSide;
use IDML\Content\Enum\Unit;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use IDML\Content\Exception\InvalidDomStructureException;
use IDML\Content\Exception\InvalidPropertyException;
use IDML\Content\SplineItem\ObjectExportOption;
use IDML\Content\SplineItem\Properties;
use IDML\Content\SplineItem\TextWrapPreference;
use ReflectionException;

/**
 * Handles rectangles
 * This class creates Rectangles on the fly.
 * The code will be returned and must be send to a Page
 * There the position of the Rectangle element will be calculated
 *
 * @package IDML\Content
 */
class Rectangle extends AbstractDOMObject implements
    DOMObjectInterface,
    PageItemInterface
{
    /**
     * Calculate points and millimeters
     */
    use CalculatorTrait;

    /**
     * Useful methods
     */
    use HelperFunctionsTrait;

    /**
     * @IDMLProperty
     */
    private string $self;

    /**
     * @IDMLProperty
     */
    private string $contentType = 'GraphicType';

    /**
     * @IDMLProperty
     */
    private ?string $storyTitle = null;

    /**
     * @IDMLProperty
     */
    private ?string $parentInterfaceChangeCount = null;

    /**
     * @IDMLProperty
     */
    private ?string $targetInterfaceChangeCount = null;

    /**
     * @IDMLProperty
     */
    private ?string $lastUpdatedInterfaceChangeCount = null;

    /**
     * @IDMLProperty
     */
    private ?string $overriddenPageItemProps = null;

    /**
     * @var string `FlexibleDimension FixedDimension FlexibleDimension`
     * @IDMLProperty
     */
    private ?string $horizontalLayoutConstraints = null;

    /**
     * @var string `FlexibleDimension FixedDimension FlexibleDimension`
     * @IDMLProperty
     */
    private ?string $verticalLayoutConstraints = null;

    /**
     * @IDMLProperty
     */
    private int|float $strokeWeight = 0;
    
    /**
     * @var string `0 0`
     * @IDMLProperty
     */
    private ?string $gradientFillStart = null;

    /**
     * @IDMLProperty
     */
    private ?int $gradientFillLength = null;

    /**
     * @IDMLProperty
     */
    private ?int $gradientFillAngle = null;

    /**
     * @var string `0 0`
     * @IDMLProperty
     */
    private ?string $gradientStrokeStart = null;

    /**
     * @IDMLProperty
     */
    private ?int $gradientStrokeLength = null;

    /**
     * @IDMLProperty
     */
    private ?int $gradientStrokeAngle = null;
    
    /**
     * @IDMLProperty
     */
    private bool $locked = false;

    /**
     * @var string `Default`
     * @IDMLProperty
     */
    private ?string $localDisplaySetting = null;

    /**
     * @IDMLProperty
     */
    private ?int $gradientFillHiliteLength = null;

    /**
     * @IDMLProperty
     */
    private ?int $gradientFillHiliteAngle = null;

    /**
     * @IDMLProperty
     */
    private ?int $gradientStrokeHiliteLength = null;

    /**
     * @IDMLProperty
     */
    private ?int $gradientStrokeHiliteAngle = null;
    
    /**
     * @IDMLProperty
     */
    private bool $visible = true;

    /**
     * @IDMLProperty
     */
    private ?string $name = null;

    /**
     * @IDMLProperty
     */
    private string $itemTransform = '1 0 0 1 0 0';
    
    /**
     * @IDMLProperty
     */
    private StrokeAlignment $strokeAlignment;
    
    private ?int $tempSortOrder = null;

    private ?TextWrapPreference $textWrapPreference = null;

    private ?ObjectExportOption $objectExportOption = null;
    
    /**
     * @var array
     */
    private $position = [
        'left' => 0,
        'top' => 0,
    ];

    /**
     * @var array
     */
    private $size = [
        'width' => 0,
        'height' => 0,
    ];

    /**
     * Creates a new DOMDocument instance
     *
     * @param string|null $uniqueId
     */
    public function __construct(string $uniqueId = null)
    {
        if (null === $uniqueId) {
            $uniqueId = $this->getUniqueId($this);
        }

        $this->setSelf($uniqueId);
        $this->setStrokeAlignment(StrokeAlignment::CENTER_ALIGNMENT);
    }

    /**
     * Sets the rotation values. Every object with a rotation rotates basically around the page middle
     * which is the zero point of our coordinates system. To archive that an object is rotated around itself
     * we're doing some calculation later when having the absolute values of all elements.
     *
     * @return Rectangle
     * @see \IDML\Content\Spread\AbstractSpread::getTransformCorrection()
     */
    public function setRotation(float $angle): self
    {
        $angle = deg2rad($angle);
        
        $itemTransform =
            cos($angle) . ' ' .
            -sin($angle) . ' ' .
            sin($angle) . ' ' .
            cos($angle) . ' ' .
            '0 0'
        ;
        
        $this->itemTransform = $itemTransform;
        return $this;
    }

    /**
     * Add the TempSortOrder index to sort elements later
     *
     * @return Rectangle
     */
    public function setIndex(int $index): self
    {
        $this->setTempSortOrder(intval($index));
        return $this;
    }

    /**
     * Setting the position of the Rectangle
     *
     * @return Rectangle
     */
    public function setPosition(float $left, float $top, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $this->position = [
            'left' => $left / $unit->getValue(),
            'top' => $top / $unit->getValue(),
        ];

        return $this;
    }

    /**
     * Setting the size of the Rectangle
     *
     * @return Rectangle
     */
    public function setSize(float $width, float $height, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $this->size = [
            'width' => $width / $unit->getValue(),
            'height' => $height / $unit->getValue(),
        ];

        return $this;
    }

    /**
     * Saves the paragraph style and returns them
     *
     * @return DOMDocument
     * @throws AnnotationException
     * @throws ReflectionException
     * @throws InvalidPropertyException
     * @throws InvalidDomStructureException
     * @throws AnnotationReaderNotInitializedException
     */
    public function render(): DOMDocument
    {
        $domDocument = self::getNewDOMDocument();

        $properties = $this->getProperties($this);

        /**
         * Create Rectangle tag
         */
        $rectangle = $domDocument->createElement('Rectangle');
        $this->setAttributes($rectangle, $properties);
        
        /**
         * Create Properties tag
         */
        $props = new Properties();
        $props
            ->setSize($this->size['width'], $this->size['height'])
            ->setPosition($this->position['left'], $this->position['top'])
        ;

        $this->appendIDMLObject($rectangle, $props, $domDocument);
        
        /**
         * Create TextWrapPreference tag
         */
        $textWrapPreference = $this->getTextWrapPreference();

        if (null === $textWrapPreference) {
            $textWrapPreference = new TextWrapPreference();
        }

        $this->appendIDMLObject($rectangle, $textWrapPreference, $domDocument);

        /**
         * Create ObjectExportOption tag
         */
        $objectExportOption = $this->getObjectExportOption();

        if (null === $objectExportOption) {
            $objectExportOption = new ObjectExportOption();
        }

        $this->appendIDMLObject($rectangle, $objectExportOption, $domDocument);
        
        $domDocument->appendChild($rectangle);
        return $domDocument;
    }

    /**
     * @return string|null
     */
    public function getSelf(): ?string
    {
        return $this->self;
    }

    /**
     * @param string $self
     * @return Rectangle
     */
    public function setSelf(string $self): self
    {
        $this->self = $self;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTempSortOrder(): ?int
    {
        return $this->tempSortOrder;
    }

    /**
     * @return Rectangle
     */
    public function setTempSortOrder(int $tempSortOrder): self
    {
        $this->tempSortOrder = $tempSortOrder;
        return $this;
    }

    /**
     * @return TextWrapPreference|null
     */
    public function getTextWrapPreference(): ?TextWrapPreference
    {
        return $this->textWrapPreference;
    }

    /**
     * @return Rectangle
     */
    public function setTextWrapPreference(TextWrapPreference $textWrapPreference): self
    {
        $this->textWrapPreference = $textWrapPreference;
        return $this;
    }

    /**
     * @return ObjectExportOption|null
     */
    public function getObjectExportOption(): ?ObjectExportOption
    {
        return $this->objectExportOption;
    }

    /**
     * @return Rectangle
     */
    public function setObjectExportOption(ObjectExportOption $objectExportOption): self
    {
        $this->objectExportOption = $objectExportOption;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    /**
     * @return Rectangle
     */
    public function setContentType(string $contentType): self
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStoryTitle(): ?string
    {
        return $this->storyTitle;
    }

    /**
     * @return Rectangle
     */
    public function setStoryTitle(string $storyTitle): self
    {
        $this->storyTitle = $storyTitle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getParentInterfaceChangeCount(): ?string
    {
        return $this->parentInterfaceChangeCount;
    }

    /**
     * @return Rectangle
     */
    public function setParentInterfaceChangeCount(string $parentInterfaceChangeCount): self
    {
        $this->parentInterfaceChangeCount = $parentInterfaceChangeCount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTargetInterfaceChangeCount(): ?string
    {
        return $this->targetInterfaceChangeCount;
    }

    /**
     * @return Rectangle
     */
    public function setTargetInterfaceChangeCount(string $targetInterfaceChangeCount): self
    {
        $this->targetInterfaceChangeCount = $targetInterfaceChangeCount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastUpdatedInterfaceChangeCount(): ?string
    {
        return $this->lastUpdatedInterfaceChangeCount;
    }

    /**
     * @return Rectangle
     */
    public function setLastUpdatedInterfaceChangeCount(string $lastUpdatedInterfaceChangeCount): self
    {
        $this->lastUpdatedInterfaceChangeCount = $lastUpdatedInterfaceChangeCount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOverriddenPageItemProps(): ?string
    {
        return $this->overriddenPageItemProps;
    }

    /**
     * @return Rectangle
     */
    public function setOverriddenPageItemProps(string $overriddenPageItemProps): self
    {
        $this->overriddenPageItemProps = $overriddenPageItemProps;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHorizontalLayoutConstraints(): ?string
    {
        return $this->horizontalLayoutConstraints;
    }

    /**
     * @return Rectangle
     */
    public function setHorizontalLayoutConstraints(string $horizontalLayoutConstraints): self
    {
        $this->horizontalLayoutConstraints = $horizontalLayoutConstraints;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVerticalLayoutConstraints(): ?string
    {
        return $this->verticalLayoutConstraints;
    }

    /**
     * @return Rectangle
     */
    public function setVerticalLayoutConstraints(string $verticalLayoutConstraints): self
    {
        $this->verticalLayoutConstraints = $verticalLayoutConstraints;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStrokeWeight(): ?int
    {
        return $this->strokeWeight;
    }

    /**
     * @param Unit|null $unit
     * @return Rectangle
     */
    public function setStrokeWeight(int $strokeWeight, Unit $unit = null): self
    {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $this->strokeWeight = $strokeWeight / $unit->getValue();

        if (null === $this->getStrokeAlignment()) {
            $this->setStrokeAlignment(StrokeAlignment::CENTER_ALIGNMENT);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGradientFillStart(): ?string
    {
        return $this->gradientFillStart;
    }

    /**
     * @return Rectangle
     */
    public function setGradientFillStart(string $gradientFillStart): self
    {
        $this->gradientFillStart = $gradientFillStart;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGradientFillLength(): ?int
    {
        return $this->gradientFillLength;
    }

    /**
     * @return Rectangle
     */
    public function setGradientFillLength(int $gradientFillLength): self
    {
        $this->gradientFillLength = $gradientFillLength;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGradientFillAngle(): ?int
    {
        return $this->gradientFillAngle;
    }

    /**
     * @return Rectangle
     */
    public function setGradientFillAngle(int $gradientFillAngle): self
    {
        $this->gradientFillAngle = $gradientFillAngle;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGradientStrokeStart(): ?string
    {
        return $this->gradientStrokeStart;
    }

    /**
     * @return Rectangle
     */
    public function setGradientStrokeStart(string $gradientStrokeStart): self
    {
        $this->gradientStrokeStart = $gradientStrokeStart;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGradientStrokeLength(): ?int
    {
        return $this->gradientStrokeLength;
    }

    /**
     * @return Rectangle
     */
    public function setGradientStrokeLength(int $gradientStrokeLength): self
    {
        $this->gradientStrokeLength = $gradientStrokeLength;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGradientStrokeAngle(): ?int
    {
        return $this->gradientStrokeAngle;
    }

    /**
     * @return Rectangle
     */
    public function setGradientStrokeAngle(int $gradientStrokeAngle): self
    {
        $this->gradientStrokeAngle = $gradientStrokeAngle;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function isLocked(): ?bool
    {
        return $this->locked;
    }

    /**
     * @return Rectangle
     */
    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocalDisplaySetting(): ?string
    {
        return $this->localDisplaySetting;
    }

    /**
     * @return Rectangle
     */
    public function setLocalDisplaySetting(string $localDisplaySetting): self
    {
        $this->localDisplaySetting = $localDisplaySetting;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGradientFillHiliteLength(): ?int
    {
        return $this->gradientFillHiliteLength;
    }

    /**
     * @return Rectangle
     */
    public function setGradientFillHiliteLength(int $gradientFillHiliteLength): self
    {
        $this->gradientFillHiliteLength = $gradientFillHiliteLength;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGradientFillHiliteAngle(): ?int
    {
        return $this->gradientFillHiliteAngle;
    }

    /**
     * @return Rectangle
     */
    public function setGradientFillHiliteAngle(int $gradientFillHiliteAngle): self
    {
        $this->gradientFillHiliteAngle = $gradientFillHiliteAngle;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGradientStrokeHiliteLength(): ?int
    {
        return $this->gradientStrokeHiliteLength;
    }

    /**
     * @return Rectangle
     */
    public function setGradientStrokeHiliteLength(int $gradientStrokeHiliteLength): self
    {
        $this->gradientStrokeHiliteLength = $gradientStrokeHiliteLength;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getGradientStrokeHiliteAngle(): ?int
    {
        return $this->gradientStrokeHiliteAngle;
    }

    /**
     * @return Rectangle
     */
    public function setGradientStrokeHiliteAngle(int $gradientStrokeHiliteAngle): self
    {
        $this->gradientStrokeHiliteAngle = $gradientStrokeHiliteAngle;
        return $this;
    }
    
    /**
     * @return bool|null
     */
    public function isVisible(): ?bool
    {
        return $this->visible;
    }

    /**
     * @return Rectangle
     */
    public function setVisible(bool $visible): self
    {
        $this->visible = $visible;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getStrokeAlignment(): ?string
    {
        return $this->strokeAlignment;
    }

    /**
     * @return Rectangle
     */
    public function setStrokeAlignment(StrokeAlignment $strokeAlignment): Rectangle
    {
        $this->strokeAlignment = $strokeAlignment;
        return $this;
    }

    /**
     * @return \IDML\Content\Rectangle
     */
    public function setTextWrapOffset(
        float $top,
        float $right,
        float $bottom,
        float $left,
        Unit $unit = null
    ): self {
        if (null === $unit) {
            $unit = Unit::POINTS;
        }

        $textWrapPreference = $this->getTextWrapPreference();

        if (null === $textWrapPreference) {
            $textWrapPreference = new TextWrapPreference();
        }

        $textWrapPreference->setTextWrapOffset($top, $right, $bottom, $left, $unit);
        $this->setTextWrapPreference($textWrapPreference);

        return $this;
    }

    /**
     * @return \IDML\Content\Rectangle
     */
    public function setTextWrapMode(TextWrapMode $textWrapMode): self
    {
        $textWrapPreference = $this->getTextWrapPreference();

        if (null === $textWrapPreference) {
            $textWrapPreference = new TextWrapPreference();
        }

        $textWrapPreference->setTextWrapMode($textWrapMode);
        $this->setTextWrapPreference($textWrapPreference);
        return $this;
    }

    /**
     * @return \IDML\Content\Rectangle
     */
    public function setTextWrapSide(TextWrapSide $textWrapSide): self
    {
        $textWrapPreference = $this->getTextWrapPreference();

        if (null === $textWrapPreference) {
            $textWrapPreference = new TextWrapPreference();
        }

        $textWrapPreference->setTextWrapSide($textWrapSide);
        $this->setTextWrapPreference($textWrapPreference);
        return $this;
    }

    /**
     * @param string $name
     * @return \IDML\Content\Rectangle
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }
}
