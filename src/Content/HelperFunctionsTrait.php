<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Content;

use DOMDocument;
use DOMElement;
use DOMNode;
use Exception;
use IDML\Content\Exception\InvalidDomStructureException;
use IDML\Content\Exception\InvalidPropertyException;
use ReflectionClass;

/**
 * Adds some useful functions
 *
 * @package IDML\Content
 */
trait HelperFunctionsTrait
{
    /**
     * Gets an DOMDocument instance
     *
     * @return DOMDocument
     */
    public static function getNewDOMDocument(): DOMDocument
    {
        $domDocument = new DOMDocument('1.0', 'UTF-8');
        $domDocument->formatOutput = true;
        $domDocument->xmlStandalone = true;
        $domDocument->preserveWhiteSpace = true;
        return $domDocument;
    }

    /**
     * Set attributes to an element
     * If an attribute is set to null it will be ignored!
     *
     * @return void
     * @throws InvalidPropertyException
     */
    public function setAttributes(DOMElement &$element, array $attributes)
    {
        foreach ($attributes as $attribute => $value) {
            if (is_array($value)) {
                throw new InvalidPropertyException($attribute);
            }

            if (is_null($value)) {
                continue;
            }

            $value = $this->boolToString($value);

            if (is_object($value)) {
                $value = $value->value;
            }

            $element->setAttribute($attribute, $value);
        }
    }
    
    /**
     * Change variables from boolean to string for xml attributes.
     * The input is mixed here so everything can pass this method. If the input is not boolean,
     * it will be returned as it is.
     *
     * @return mixed
     */
    public function boolToString(mixed $input)
    {
        if (is_bool($input)) {
            return true === $input ? 'true' : 'false';
        }

        return $input;
    }

    /**
     * Imports and appends an DOMObject by calling its render() function
     *
     * @param DOMDocument|DOMElement $node
     * @param DOMDocument|null $document
     * @throws InvalidDomStructureException
     */
    public function appendIDMLObject(
        $node,
        NestedDOMObjectInterface $object,
        DOMDocument $document = null
    ) {
        /** @var DOMDocument $root */
        $root = $document ?? $node;
        
        $element = $object->render()->documentElement;

        if (!$element instanceof DOMNode) {
            throw new InvalidDomStructureException();
        }
        
        $child = $root->importNode(
            $element,
            true
        );
        
        $node->appendChild($child);
    }

    /**
     * @return string
     */
    public function getUniqueId(NestedDOMObjectInterface $class): string
    {
        try {
            $class = new ReflectionClass($class);
            $prefix = $class->getShortName();
        } catch (Exception) {
            $prefix = $class::class;
        }
        
        return uniqid($prefix);
    }
}
