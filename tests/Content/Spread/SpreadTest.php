<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Tests\Content\Spread;

use BitAndBlack\Composer\Exception\VendorNotFoundException;
use Doctrine\Common\Annotations\AnnotationException;
use DOMElement;
use DOMNodeList;
use IDML\Content;
use IDML\Content\Enum\Spread\PagePosition;
use IDML\Content\Exception\AnnotationReaderNotInitializedException;
use IDML\Content\Exception\InvalidPropertyException;
use IDML\Content\Exception\PageSizeException;
use IDML\Content\Page;
use IDML\Content\Spread\Spread;
use PHPUnit\Framework\TestCase;
use ReflectionException;

/**
 * Class SpreadTest
 *
 * @package IDML\Tests\Content\Spread
 */
class SpreadTest extends TestCase
{
    /**
     * Tests if pages in the same spread return correct GeometricBound and ItemTransform values
     *
     * @throws VendorNotFoundException
     * @throws AnnotationException
     * @throws AnnotationReaderNotInitializedException
     * @throws InvalidPropertyException
     * @throws PageSizeException
     * @throws ReflectionException
     */
    public function testCanPlacePages()
    {
        $pageValues = [
            0 => [
                'GeometricBounds' => '100 50 300 150',
                'ItemTransform' => '1 0 0 1 -225 -200',
            ],
            1 => [
                'GeometricBounds' => '0 0 400 200',
                'ItemTransform' => '1 0 0 1 -75 -200',
            ],
            2 => [
                'GeometricBounds' => '175 75 225 125',
                'ItemTransform' => '1 0 0 1 50 -200',
            ],
        ];

        $content = new Content();

        $page1 = new Page('Page1');
        $page1->setFormat(100, 200);

        $page2 = new Page('Page2');
        $page2->setFormat(200, 400);

        $page3 = new Page('Page3');
        $page3->setFormat(50, 50);

        $spread = new Spread();
        $spread
            ->insertPage($page1, PagePosition::RIGHT_OUTER)
            ->insertPage($page2, PagePosition::RIGHT_OUTER)
            ->insertPage($page3, PagePosition::RIGHT_OUTER)
        ;

        $rendered = $spread->render();

        /** @var DOMElement $spreadElement */
        $spreadElement = $rendered->getElementsByTagName('Spread')[0];

        /** @var DOMNodeList $pageElements */
        $pageElements = $rendered->getElementsByTagName('Page');

        $this->assertSame(
            3,
            (int) $spreadElement->getAttribute('PageCount')
        );

        $this->assertSame(
            3,
            $pageElements->count()
        );

        /**
         * @var DOMElement $page
         */
        foreach ($pageElements as $key => $page) {
            $this->assertSame(
                $page->getAttribute('GeometricBounds'),
                $pageValues[$key]['GeometricBounds']
            );
            $this->assertSame(
                $page->getAttribute('ItemTransform'),
                $pageValues[$key]['ItemTransform']
            );
        }

        unset($content);
    }
}
