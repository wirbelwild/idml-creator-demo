<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Tests\Content;

use IDML\Content\Enum\Unit;
use IDML\Content\Exception\ColumnsException;
use IDML\Content\Exception\PageSizeException;
use IDML\Content\Page;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

/**
 * Class PageTest
 *
 * @package IDML\Tests\Content
 */
class PageTest extends TestCase
{
    /**
     * @throws PageSizeException
     */
    #[DataProvider('pageSizeProvider')]
    public function testThrowsExceptionOnWrongSize(float $width, float $height): void
    {
        $this->expectException(PageSizeException::class);
        $page = new Page();
        $page->setFormat($width, $height);
    }

    /**
     * @return Iterator
     */
    public static function pageSizeProvider(): Iterator
    {
        yield [0, 0];
        yield [20000, 0];
        yield [0, 20000];
        yield [20000, 20000];
    }

    /**
     * @throws PageSizeException
     * @throws ColumnsException
     */
    public function testThrowsColumnException(): void
    {
        $this->expectException(ColumnsException::class);
        $page = new Page();
        $page
            ->setFormat(210, 297, Unit::MILLIMETERS)
            ->setColumns(4, 10, Unit::MILLIMETERS)
            ->setMargin(100, 100, 100, 100, Unit::MILLIMETERS)
        ;
    }
}
