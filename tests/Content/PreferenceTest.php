<?php

/**
 * IDML-Creator DEMO
 *
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * You are allowed to use this code for your testing purposes
 *
 * @copyright Copyright (c) Bit&Black
 * @author Tobias Köngeter <hello@bitandblack.com>
 * @link https://www.bitandblack.com
 */

namespace IDML\Tests\Content;

use IDML\Content\Exception\PageSizeException;
use IDML\Content\Preference;
use Iterator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

/**
 * Class PreferenceTest
 *
 * @package IDML\Tests\Content
 */
class PreferenceTest extends TestCase
{
    /**
     * @throws PageSizeException
     */
    #[DataProvider('pageSizeProvider')]
    public function testThrowsExceptionOnWrongSize(float $width, float $height)
    {
        $this->expectException(PageSizeException::class);
        $page = new Preference();
        $page->setDocumentFormat($width, $height);
    }

    /**
     * @return array
     */
    public static function pageSizeProvider(): Iterator
    {
        yield [0, 0];
        yield [20000, 0];
        yield [0, 20000];
        yield [20000, 20000];
    }
}
