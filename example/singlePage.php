<?php

use IDML\Content;
use IDML\Content\BackingStory;
use IDML\Content\Container;
use IDML\Content\DesignMap;
use IDML\Content\Enum\Spread\PagePosition as PagePositionEnum;
use IDML\Content\Enum\Unit as UnitEnum;
use IDML\Content\Page;
use IDML\Content\Preference;
use IDML\Content\Rectangle;
use IDML\Content\Spread\Spread;
use IDML\Writer\File\Tree;
use IDML\Writer\Writer;

/**
 * Requires composers autoload
 */
require '../vendor/autoload.php';

/**
 * Sets up a new document
 */
$content = new Content();
$content
    ->addBackingStory(new BackingStory())
    ->addContainer(new Container())
    ->addDesignMap(new DesignMap())
;

/**
 * Defines some basics
 */
$preference = new Preference();
$preference
    ->setSinglePage(true)
    ->setDocumentFormat(210, 297, UnitEnum::MILLIMETERS)
    ->setDocumentBleed(3, UnitEnum::MILLIMETERS)
;

/**
 * Creates a Rectangle
 */
$rectangle = new Rectangle();
$rectangle
    ->setPosition(20, 20, UnitEnum::MILLIMETERS)
    ->setSize(60, 80, UnitEnum::MILLIMETERS)
;

/**
 * Adds pages
 */
$page = new Page();
$page
    ->setFormat(210, 297, UnitEnum::MILLIMETERS)
    ->addRectangle($rectangle)
    ->setMargin(20, 20, 20, 20, UnitEnum::MILLIMETERS)
    ->setColumns(1, 10, UnitEnum::MILLIMETERS)
;

/**
 * Adds a spread
 */
$spread = new Spread();
$spread->insertPage($page, PagePositionEnum::RIGHT);

$content
    ->addPreference($preference)
    ->addSpread($spread)
;

$outputFile = __DIR__ . DIRECTORY_SEPARATOR . 'singlePage.idml';

/**
 * Sets up the writer object and creates an IDML file
 */
$writer = new Writer(
    $content,
    new Tree()
);
$writer->create($outputFile);

echo 'Successfully created file://' . $outputFile . '.' . PHP_EOL;
